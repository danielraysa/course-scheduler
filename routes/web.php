<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/qwe', function () {
    return phpinfo();
});

Route::get('/starter', function () {
    return view('starter');
});

Auth::routes(['register' => false, 'reset' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    // Route::prefix('admin')->group(function () {
    Route::group(['middleware' => 'admin', 'prefix' => 'admin',], function () {
        Route::resource('konfigurasi', 'KonfigurasiController');
        Route::resource('konstrain', 'KonstrainController');
        Route::resource('user', 'UserController');
        Route::resource('siswa', 'SiswaController');
        Route::resource('guru', 'GuruController');
        Route::post('guru/{id}/aktifkan', 'GuruController@aktifkan')->name('guru.aktifkan');
        Route::post('siswa/{id}/aktifkan', 'SiswaController@aktifkan')->name('siswa.aktifkan');
        Route::post('mata-pelajaran/{id}/aktifkan', 'MataPelajaranController@aktifkan')->name('mata-pelajaran.aktifkan');
        Route::post('kelas/{id}/aktifkan', 'KelasController@aktifkan')->name('kelas.aktifkan');
        Route::resource('wali-kelas', 'WaliKelasController');
        Route::resource('mata-pelajaran', 'MataPelajaranController');
        Route::resource('kelas', 'KelasController');
        Route::resource('penempatan', 'PenempatanKelasController');
        Route::resource('semester', 'SemesterController');
        Route::resource('asrama', 'AsramaController');
        Route::resource('jadwal-pelajaran', 'JadwalPelajaranController');
        // Route::get('penjadwalan', 'PenjadwalanController@index')->name('penjadwalan.index');
        Route::get('penjadwalan/tabel', 'PenjadwalanController@showList')->name('penjadwalan.kelas');
        // Route::post('penjadwalan', 'PenjadwalanController@store')->name('penjadwalan.store');
        Route::resource('penjadwalan', 'PenjadwalanController');
        // Route::resource('penilaian', 'NilaiSiswaController');
    });
    // Route::group(['middleware' => ['guru','admin']], function () {
        Route::get('presensi', 'PresensiController@index')->name('presensi.index');
        Route::get('presensi/detail', 'PresensiController@list_presensi_siswa')->name('presensi.detail');
        Route::get('get-siswa', 'PresensiController@create')->name('presensi.get-siswa');
        Route::post('presensi', 'PresensiController@store')->name('presensi.store');
        Route::get('presensi/list', 'PresensiController@showList')->name('presensi.list');
        Route::get('presensi/list/{id}', 'PresensiController@show')->name('presensi.show');
        Route::get('presensi/susulan', 'PresensiController@susulan')->name('presensi.susulan');
        Route::post('presensi/susulan/simpan', 'PresensiController@simpan_susulan')->name('presensi.simpan-susulan');

        Route::get('penilaian', 'NilaiSiswaController@index')->name('penilaian.index');
        Route::get('get-list-siswa', 'NilaiSiswaController@create')->name('penilaian.get-siswa');
        Route::post('penilaian', 'NilaiSiswaController@store')->name('penilaian.store');
        Route::post('penilaian/update', 'NilaiSiswaController@update_nilai')->name('penilaian.update_nilai');
    // });
    Route::get('report', 'RaporController@report_nilai')->name('report-nilai');
    Route::get('rapor', 'RaporController@index')->name('rapor');
    Route::get('rapor/{id}/detail', 'RaporController@show')->name('rapor.detail');
    Route::get('rapor/{id}/cetak', 'RaporController@cetak')->name('rapor.cetak');
    Route::get('laporan/presensi', 'LaporanController@presensi')->name('laporan.presensi');
    Route::get('laporan/nilai', 'LaporanController@raport')->name('laporan.raport');

    Route::prefix('guru')->group(function () {
        Route::resource('jadwal', 'JadwalPelajaranController');
    });
    Route::prefix('siswa')->group(function () {
        Route::resource('jadwal', 'JadwalPelajaranController');
    });
});
