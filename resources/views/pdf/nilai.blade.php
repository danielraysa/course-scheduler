<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Nilai</title>
    <style>
        .bold {
            font-weight: bold
        }
        .text-center {
            text-align: center
        }
    </style>
</head>
<body>
    <table style="width: 100%">
        <tr>
            <td style="text-align: right;">
                <img src="{{ asset('logo-smk-telkom.png') }}" style="width: 90px" />
            </td>
            <td style="text-align: center">
                <h3 style="text-align: center; margin-bottom: 0">LAPORAN PENILAIAN</h3>
                <h3 style="text-align: center; margin-top: 0.5rem">SMK TELEKOMUNIKASI DARUL 'ULUM JOMBANG</h4>
            </td>
        </tr>
    </table>
    <hr>
    <h4 style="text-align: center; margin-bottom: 0"><strong>{{ $kelas->jns_kelas->nama_jenis }} - {{ $kelas->nama_kelas }}</strong></h4>
    <h4 style="text-align: center; margin-top: 0"><strong>Mata Pelajaran : {{ $matpel->nama_matpel }}</strong></h4>
    <table border="1" cellpadding="5" style="border-collapse: collapse; table-layout:fixed; width: 100%;">
        <tr>
            <th style="width: 5%">No.</th>
            <th style="width: 35%">Nama Siswa</th>
            <th>Tugas</th>
            <th>Harian</th>
            <th>UTS</th>
            <th>UAS</th>
            <th>Nilai Akhir</th>
        </tr>
        @foreach ($siswa as $item)
        <tr class="text-center">
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->nama_siswa }}</td>
            @php
                $nilai = App\Helper\KalkulasiNilai::averageNilai($item, $persentase);
                // dd($nilai);
            @endphp
            <td>{{ $nilai['rata_tugas'] }}</td>
            <td>{{ $nilai['rata_harian'] }}</td>
            <td>{{ $nilai['rata_uts'] }}</td>
            <td>{{ $nilai['rata_uas'] }}</td>
            <td>{{ $nilai['nilai_akhir'] }}</td>
        </tr>
        @endforeach
    </table>
</body>
</html>
