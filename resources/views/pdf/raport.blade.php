<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Rapor</title>
    <style>
        body {
            font-size: 12px
        }
        .bold {
            font-weight: bold
        }
        .text-center {
            text-align: center
        }
    </style>
</head>
<body>
    <table style="width: 100%">
        <tr>
            <td style="text-align: right;">
                <img src="{{ asset('logo-smk-telkom.png') }}" style="width: 90px" />
            </td>
            <td style="text-align: center">
                <h3 style="text-align: center; margin-bottom: 0">LAPORAN HASIL BELAJAR PESERTA DIDIK</h3>
                <h3 style="text-align: center; margin-top: 0.5rem">SMK TELEKOMUNIKASI DARUL 'ULUM JOMBANG</h4>
            </td>
        </tr>
    </table>
    <hr>
    <h4 style="text-align: center"><strong>{{ $kelas->jns_kelas->nama_jenis }} - Semester Ganjil</strong></h4>
    <table style="border-collapse: collapse; table-layout:fixed; width: 100%; margin-bottom: 1rem">
        <tr>
            <td>Nama Peserta Didik</td>
            <td style="width: 2%">:</td>
            <td>{{ $siswa->nama_siswa }}</td>
            <td style="width: 5%"></td>
            <td>Semester</td>
            <td style="width: 2%">:</td>
            <td>{{ $semester->semester }}</td>
        </tr>
        <tr>
            <td>Nomor Induk</td>
            <td>:</td>
            <td>{{ $siswa->nis }}</td>
            <td></td>
            <td>Tahun Ajaran</td>
            <td>:</td>
            <td>{{ $semester->periode }}</td>
        </tr>
        <tr>
            <td>Kelas</td>
            <td>:</td>
            <td>{{ $kelas->nama_kelas }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table border="1" cellpadding="5" style="border-collapse: collapse; table-layout:fixed; width: 100%;">
        <tr>
            <th rowspan="2" class="text-center" style="width: 10%">No.</th>
            <th rowspan="2" class="text-center" style="width: 35%">Mata Pelajaran</th>
            <th rowspan="2" class="text-center" style="width: 10%">KKM</th>
            <th colspan="2" class="text-center">Nilai Hasil Belajar Siswa</th>
        </tr>
        <tr>
            <th class="text-center" style="width: 10%">Angka</th>
            <th class="text-center">Huruf</th>
        </tr>
        <tr>
            <th colspan="2" class="text-left">MATPEL KEAGAMAAN</th>
            <th colspan="3" ></th>
        </tr>
        @php
            $total_nilai = 0;
        @endphp
        @foreach ($matpel_keagamaan as $item)
        <tr>
            <td class="text-center">{{ $loop->iteration }}</td>
            <td>{{ $item->nama_matpel }}</td>
            <td class="text-center">{{ $item->nilai_kkm }}</td>
            <td class="text-center">{{ $item->nilai['nilai_akhir'] }}</td>
            <td>{{ App\Helper\KalkulasiNilai::numberSpellout(round($item->nilai['nilai_akhir'], 2)) }}</td>
        </tr>
        @php
            $total_nilai += round($item->nilai['nilai_akhir'], 2);
        @endphp
        @endforeach
        <tr>
            <th colspan="2" class="text-left">MATPEL UMUM</th>
            <th colspan="3" ></th>
        </tr>
        @foreach ($matpel_umum as $item)
        <tr>
            <td class="text-center">{{ $loop->iteration }}</td>
            <td>{{ $item->nama_matpel }}</td>
            <td class="text-center">{{ $item->nilai_kkm }}</td>
            <td class="text-center">{{ $item->nilai['nilai_akhir'] }}</td>
            <td>{{ App\Helper\KalkulasiNilai::numberSpellout(round($item->nilai['nilai_akhir'], 2)) }}</td>
        </tr>
        @php
            $total_nilai += round($item->nilai['nilai_akhir'], 2);
        @endphp
        @endforeach
        {{-- @for ($i = 0; $i < 10; $i++)
        <tr>
            <td class="text-center">1</td>
            <td>Pendidikan Agama</td>
            <td class="text-center">75</td>
            <td class="text-center">90</td>
            <td>Sembilan puluh</td>
        </tr>
        @endfor --}}
        @php
            $rata_rata = round($total_nilai/($matpel_keagamaan->count()+$matpel_umum->count()), 2);
        @endphp
        <tr>
            <td colspan="2" class="text-center bold">JUMLAH</td>
            <td></td>
            <td class="text-center">{{ $total_nilai }}</td>
            <td class="text-center"></td>
            {{-- <td class="text-center">{{ App\Helper\KalkulasiNilai::numberSpellout($total_nilai) }}</td> --}}
        </tr>
        <tr>
            <td colspan="2" class="text-center bold">RATA-RATA</td>
            <td></td>
            <td class="text-center">{{ $rata_rata }}</td>
            <td class="text-center">{{ App\Helper\KalkulasiNilai::numberSpellout($rata_rata) }}</td>
        </tr>
    </table>
    <h4>Jumlah Ketidakhadiran</h4>
    <table border="1" cellpadding="5" style="border-collapse: collapse; table-layout:fixed; width: 60%;">
        <tr>
            <td>Sakit</td>
            <td>{{ $presensi_sakit }}</td>
        </tr>
        <tr>
            <td>Ijin</td>
            <td>{{ $presensi_ijin }}</td>
        </tr>
        <tr>
            <td>Alpha</td>
            <td>{{ $presensi_alpha }}</td>
        </tr>
    </table>
</body>
</html>
