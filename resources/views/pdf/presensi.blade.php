<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Presensi</title>
    <style>
        .bold {
            font-weight: bold
        }
        .text-center {
            text-align: center
        }
    </style>
</head>
<body>
    <table style="width: 100%">
        <tr>
            <td style="text-align: center">
                <img src="{{ asset('logo-smk-telkom.png') }}" style="width: 110px" />
            </td>
            <td>
                <h3 style="text-align: center; margin-bottom: 0">LAPORAN PRESENSI</h3>
                <h3 style="text-align: center; margin-top: 0.5rem;">SMK TELEKOMUNIKASI DARUL 'ULUM JOMBANG</h3>
            </td>
        </tr>
    </table>
    <hr>
    <h4 style="text-align: center; margin-bottom: 0"><strong>{{ $kelas->jns_kelas->nama_jenis }} - {{ $kelas->nama_kelas }}</strong></h4>
    <h4 style="text-align: center; margin-top: 0"><strong>Periode : {{ date('F Y', strtotime('01-'.$bulan.'-'.$tahun)) }}</strong></h4>

    <table border="1" cellpadding="5" style="border-collapse: collapse; table-layout:fixed; width: 100%;">
        <tr class="text-center">
            <th style="width: 10%">No.</th>
            <th>Nama Siswa</th>
            <th style="width: 10%">Hadir</th>
            <th style="width: 10%">Alpha</th>
            <th style="width: 10%">Ijin</th>
            <th style="width: 10%">Sakit</th>
        </tr>
        @foreach ($data_presensi as $item)
        <tr>
            <td class="text-center">{{ $loop->iteration }}</td>
            <td>{{ $item->nama_siswa }}</td>
            <td>{{ $item->hadir }}</td>
            <td>{{ $item->alpha }}</td>
            <td>{{ $item->ijin }}</td>
            <td>{{ $item->sakit }}</td>
        </tr>
        @endforeach
    </table>
    
</body>
</html>
