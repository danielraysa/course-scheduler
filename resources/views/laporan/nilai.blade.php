@extends('layouts.admin', ['halaman' => 'Laporan Nilai '.(isset($data_kelas) ? $data_kelas->nama_kelas : '')])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({

        });
        $('#example2').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            // "autoWidth": false,
            // "responsive": true,
        });
    });
    $(".alert").alert();
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="GET">
                    <div class="row mb-3">
                        <div class="col-lg-3">
                            <label>Semester</label>
                            <select name="semester" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Semeseter --</option>
                                @foreach ($semester as $item)
                                    <option value="{{ $item->id }}" {{ isset($request) && $request->semester == $item->id ? 'selected' : '' }}>{{ $item->periode }} - {{ $item->semester }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label>Kelas</label>
                            <select name="kelas" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Kelas --</option>
                                @foreach ($kelas as $item)
                                    <option value="{{ $item->id }}" {{ isset($request) && $request->kelas == $item->id ? 'selected' : '' }}>{{ $item->nama_kelas }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label>Mata Pelajaran</label>
                            <select name="matpel" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Mata Pelajaran --</option>
                                @foreach ($matpel as $item)
                                    <option value="{{ $item->id }}" {{ isset($request) && $request->matpel == $item->id ? 'selected' : '' }}>{{ $item->nama_matpel }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <button type="submit" class="btn btn-primary mt-4">Filter</button>
                            <button type="submit" name="cetak" value="true" class="btn btn-success mt-4">Cetak</button>
                        </div>
                        </form>
                    </div>
                    @if(isset($siswa))
                    <div class="row mb-3">
                        <div class="col-md-3 col-6">
                            <div class="card bg-primary">
                                <div class="card-body">
                                    <p class="mb-0">Rata-rata Nilai Akhir</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $rata_nilai ?? 0 }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="card bg-danger">
                                <div class="card-body">
                                    <p class="mb-0">Nilai Akhir Terendah</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $nilai_terendah ?? 0 }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="card bg-success">
                                <div class="card-body">
                                    <p class="mb-0">Nilai Akhir Tertinggi</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $nilai_tertinggi ?? 0 }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr class="text-center">
                                    <th>No.</th>
                                    <th>NIS</th>
                                    <th>Nama Siswa</th>
                                    <th>Tugas</th>
                                    <th>Harian</th>
                                    <th>UTS</th>
                                    <th>UAS</th>
                                    <th>Nilai Akhir</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($siswa as $item)
                                <tr class="text-center">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nis }}</td>
                                    <td>{{ $item->nama_siswa }}</td>
                                    @php
                                        $nilai = App\Helper\KalkulasiNilai::averageNilai($item, $persentase);
                                        // dd($nilai);
                                    @endphp
                                    <td>{{ $nilai['rata_tugas'] }}</td>
                                    <td>{{ $nilai['rata_harian'] }}</td>
                                    <td>{{ $nilai['rata_uts'] }}</td>
                                    <td>{{ $nilai['rata_uas'] }}</td>
                                    <td>{{ $nilai['nilai_akhir'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
