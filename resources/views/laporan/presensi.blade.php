@extends('layouts.admin', ['halaman' => 'Laporan Presensi '.$kelas->nama_kelas])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({

        });
        $('#example2').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            // "autoWidth": false,
            // "responsive": true,
        });
    });
    $(".alert").alert();
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="GET">
                    <div class="row mb-3">
                        <div class="col-lg-3 col-6">
                            <label>Bulan</label>
                            <input type="month" name="bulan" class="form-control" value="{{ $filter }}" />
                        </div>
                        <div class="col-3">
                            <button type="submit" class="btn btn-primary mt-4">Filter</button>
                            <button type="submit" name="cetak" value="true" class="btn btn-success mt-4">Cetak</button>
                        </div>
                        </form>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-3 col-12">
                            <div class="card bg-info">
                                <div class="card-body">
                                    <p class="mb-0">Kehadiran</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $presentasi_hadir }} %</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="card bg-danger">
                                <div class="card-body">
                                    <p class="mb-0">Alpha</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $presentasi_alpha }} %</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="card bg-warning">
                                <div class="card-body">
                                    <p class="mb-0">Ijin</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $presentasi_ijin }} %</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="card bg-primary">
                                <div class="card-body">
                                    <p class="mb-0">Sakit</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $presentasi_sakit }} %</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr class="text-center">
                                <th>No.</th>
                                <th>NIS</th>
                                <th>Nama Siswa</th>
                                <th>Hadir</th>
                                <th>Alpha</th>
                                <th>Ijin</th>
                                <th>Sakit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_presensi as $item)
                            <tr class="text-center">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nis }}</td>
                                <td>{{ $item->nama_siswa }}</td>
                                <td>{{ $item->hadir }}</td>
                                <td>{{ $item->alpha }}</td>
                                <td>{{ $item->ijin }}</td>
                                <td>{{ $item->sakit }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
