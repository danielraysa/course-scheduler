@extends('layouts.admin', ['halaman' => 'Penempatan Siswa-Kelas'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
<style>
    .select2-container .select2-selection--single {
        height: calc(1.5em + 0.75rem + 2px) !important;
    }
    /* .select2-container .select2-selection--single .select2-selection__rendered {
        line-height: 38px;
        padding-left: 1rem;
    } */
    .select2-container .select2-selection--single .select2-selection__arrow{
        height: 34px;
    }
</style>
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
    $('.select2').select2();
  });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <form action="" method="GET">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Semester</label>
                            <select name="semester" class="form-control">
                                <option value="" selected disabled>-- Pilih Semester --</option>
                                @foreach ($semester as $item)
                                <option value="{{ $item->id }}" {{ isset($request->semester) && $request->semester == $item->id ? 'selected' : '' }}>{{ $item->periode }} - {{ $item->semester }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Kelas</label>
                            <select name="kelas" id="kelas" class="form-control">
                                <option value="" selected disabled>-- Pilih Kelas --</option>
                                @foreach ($kelas as $item)
                                <option value="{{ $item->id }}" {{ isset($request->kelas) && $request->kelas == $item->id ? 'selected' : '' }}>{{ $item->nama_kelas }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <button type="submit" class="btn btn-primary mx-3 float-lg-left">
                            Filter
                        </button>
                        <button type="button" class="btn btn-primary float-lg-left" data-toggle="modal" data-target="#modalTambah">
                            Tambah Data
                        </button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        @if(isset($penempatan))
        <div class="card">
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No.</th>
                            <th>Periode/Semester</th>
                            <th>Kelas</th>
                            {{-- <th>Asrama</th> --}}
                            <th>Nama Siswa</th>
                            {{-- <th></th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($penempatan as $item)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td>{{ $item->data_semester->periode." - ".$item->data_semester->semester }}</td>
                            <td>{{ $item->data_kelas->nama_kelas }}</td>
                            {{-- <td>{{ $item->data_asrama->nama_asrama }}</td> --}}
                            <td>{{ $item->data_siswa->nama_siswa }}</td>
                            {{-- <td class="text-center">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#" data-id="{{ $item->id }}"><i class="fas fa-edit"></i> Edit</button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#" data-id="{{ $item->id }}"><i class="fas fa-trash"></i> Hapus</button>
                            </td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>
</div>
<!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('penempatan.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Data Penempatan Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Semester</label>
                    <select name="semester" id="semester" class="form-control">
                        <option value="" selected disabled>-- Pilih Semester --</option>
                        @foreach ($semester as $item)
                        <option value="{{ $item->id }}">{{ $item->periode }} - {{ $item->semester }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>NIS - Siswa</label>
                    <select name="siswa" id="siswa" class="form-control select2" style="width: 100%">
                        <option value="" selected disabled>-- Pilih Siswa --</option>
                        @foreach ($siswa as $item)
                        <option value="{{ $item->id }}">{{ $item->nis }} - {{ $item->nama_siswa }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Kelas</label>
                    <select name="kelas" id="kelas" class="form-control select2" style="width: 100%">
                        <option value="" selected disabled>-- Pilih Kelas --</option>
                        @foreach ($kelas as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Asrama</label>
                    <select name="asrama" id="asrama" class="form-control select2" style="width: 100%">
                        <option value="" selected disabled>-- Pilih Asrama --</option>
                        @foreach ($asrama as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_asrama }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
