<table class="table">
    <tr>
        <th>No.</th>
        <th>Jam Pelajaran</th>
        <th>Mata Pelajaran</th>
        <th>Guru</th>
        <th></th>
    </tr>
    @foreach ($jadwal_kelas as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->jam_matpel }}</td>
            <td>{{ $item->data_matpel->nama_matpel }}</td>
            <td>{{ $item->data_guru->nama_guru }}</td>
            <td>
                <button class="btn btn-warning" data-toggle="modal" data-target="#modalUpdate-{{ $item->id }}"> Edit</button>
                <button class="btn btn-danger" data-toggle="modal" data-target="#modalHapus-{{ $item->id }}"> Hapus</button>
            </td>
        </tr>
    @endforeach
</table>
@foreach ($jadwal_kelas as $jdw)
<div class="modal fade" id="modalUpdate-{{ $jdw->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('penjadwalan.update', $jdw->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-header">
                <h5 class="modal-title">Update Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Kelas</label>
                    <select name="kelas" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Kelas --</option>
                        @foreach ($kelas as $item)
                        <option {{ $item->id == $jdw->kelas_id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Hari</label>
                    <select name="hari" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Hari --</option>
                        @foreach ($harian as $item)
                        <option {{ $item->hari == $jdw->hari ? 'selected' : '' }} value="{{ $item->hari }}">{{ $item->hari }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Jam ke</label>
                    <select name="jam" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Jam --</option>
                        @foreach (range(1, 10) as $item)
                        <option {{ $item == $jdw->jam_matpel ? 'selected' : '' }} value="{{ $item }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Mata Pelajaran</label>
                    <select name="matpel" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Matpel --</option>
                        @foreach ($matpel as $item)
                        <option {{ $item->id == $jdw->matpel_id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->nama_matpel }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Guru</label>
                    <select name="guru" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Guru --</option>
                        @foreach ($guru as $item)
                        <option {{ $item->id == $jdw->guru_id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->nama_guru }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalHapus-{{ $jdw->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('penjadwalan.destroy', $jdw->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-header">
                <h5 class="modal-title">Update Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah mau menghapus jadwal ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
