@extends('layouts.admin', ['halaman' => 'Jadwal Mata Pelajaran'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
    $('#filter').click(function(){
        var id = $('#kelas').val();
        var hari = $('#hari').val();
        $.ajax({
            url:"{{ route('penjadwalan.kelas') }}",
            type: "GET",
            data: {kelas: id, hari: hari},
            success: function(result){
                $('#list-table').html(result);
            },
            error: function(err){
                $('#list-table').html(`<p>Error</p>`);
            }
        });
    });
  });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body table-responsive">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Kelas</label>
                            <select name="kelas" id="kelas" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Kelas --</option>
                                @foreach ($kelas as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Hari</label>
                            <select name="hari" id="hari" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Hari --</option>
                                @foreach ($harian as $item)
                                <option value="{{ $item->hari }}">{{ $item->hari }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <button class="btn btn-success" id="filter">Filter</button>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#modalTambah">Tambah</button>
                        </div>
                    </div>
                </div>
                <div id="list-table">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('penjadwalan.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Data Jadwal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Kelas</label>
                    <select name="kelas" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Kelas --</option>
                        @foreach ($kelas as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Hari</label>
                    <select name="hari" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Hari --</option>
                        @foreach ($harian as $item)
                        <option value="{{ $item->hari }}">{{ $item->hari }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Jam ke</label>
                    <select name="jam" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Jam --</option>
                        @foreach (range(1, 10) as $item)
                        <option value="{{ $item }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Mata Pelajaran</label>
                    <select name="matpel" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Matpel --</option>
                        @foreach ($matpel as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_matpel }} (bobot {{ $item->bobot_matpel }})</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Guru</label>
                    <select name="guru" class="form-control select2" style="width: 100%" required>
                        <option value="" selected disabled>-- Pilih Guru --</option>
                        @foreach ($guru as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_guru }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection
