@extends('layouts.admin', ['halaman' => 'Data Wali Kelas'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $('.select2').select2();
    $("#example1").DataTable();
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
    $('.checkbox-user').change(function(){
        var value = $(this).is(':checked');
        if(value){
            $('.user-create-box').show();
        }else{
            $('.user-create-box').hide();
        }
    });
    $('.checkbox-user-edit').change(function(){
        var value = $(this).is(':checked');
        if(value){
            $('.user-edit-box').show();
        }else{
            $('.user-edit-box').hide();
        }
    });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-primary float-lg-left" data-toggle="modal" data-target="#modalTambah">
                    Tambah Data
                </button>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No.</th>
                            <th>Semester</th>
                            <th>Nama Guru</th>
                            <th>Kelas</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($walikelas as $item)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="text-center">{{ $item->data_semester->periode }} ({{ $item->data_semester->jenis_semester() }})</td>
                            <td>{{ $item->data_guru->nama_guru }}</td>
                            <td>{{ $item->data_kelas->nama_kelas }}</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalEdit-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-edit"></i> Edit</button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalHapus-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-trash"></i> Hapus</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('wali-kelas.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Data Wali Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Semester</label>
                    <select name="semester" class="form-control select2" style="width: 100%">
                        @foreach ($semester as $item)
                        <option value="{{ $item->id }}">{{ $item->periode }} ({{ $item->jenis_semester() }})</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Guru</label>
                    <select name="guru" class="form-control select2" style="width: 100%">
                        @foreach ($guru as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_guru }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Kelas</label>
                    <select name="kelas" class="form-control select2" style="width: 100%">
                        @foreach ($kelas as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($walikelas as $wali)
<div class="modal fade" id="modalEdit-{{ $wali->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('wali-kelas.update', $wali) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-header">
                <h5 class="modal-title">Form Ubah Data Wali Kelas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Semester</label>
                    <select name="semester" class="form-control select2" style="width: 100%">
                        @foreach ($semester as $item)
                        <option {{ $item->id == $wali->semester_id ? 'selected' : ''  }} value="{{ $item->id }}">{{ $item->periode }} ({{ $item->jenis_semester() }})</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Guru</label>
                    <select name="guru" class="form-control select2" style="width: 100%">
                        @foreach ($guru as $item)
                        <option {{ $item->id == $wali->guru_id ? 'selected' : ''  }} value="{{ $item->id }}">{{ $item->nama_guru }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Kelas</label>
                    <select name="kelas" class="form-control select2" style="width: 100%">
                        @foreach ($kelas as $item)
                        <option {{ $item->id == $wali->kelas_id ? 'selected' : ''  }} value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalHapus-{{ $wali->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('wali-kelas.destroy', $wali) }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-header">
                <h5 class="modal-title">Hapus Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Apakah mau menghapus data wali kelas <strong>{{ $wali->data_kelas->nama_kelas }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary">Ya</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection
