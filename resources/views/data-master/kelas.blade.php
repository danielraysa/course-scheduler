@extends('layouts.admin', ['halaman' => 'Data Kelas'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({

        });
        $('#example2').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            // "autoWidth": false,
            // "responsive": true,
        });
    });
    $(".alert").alert();
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="row">
        <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalTambahJenis">
                    Tambah Jenis
                </button> --}}
                <h5>Nama Jurusan Kelas</h5>
                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            {{-- <th>No.</th> --}}
                            <th>Jurusan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jenis_kelas as $item)
                        <tr class="text-center">
                            {{-- <td>{{ $loop->iteration }}</td> --}}
                            <td>{{ $item->nama_jenis }}</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modalEditJenis-{{ $item->id }}"><i class="fas fa-edit"></i></button>
                                {{-- <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modalHapus-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-trash"></i></button> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
        <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-primary float-lg-left" data-toggle="modal" data-target="#modalTambah">
                    Tambah Kelas
                </button>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No.</th>
                            <th>Nama Kelas</th>
                            <th>Jurusan</th>
                            <th>Kuota Kelas</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kelas as $item)
                        <tr class="text-center">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->nama_kelas }}</td>
                            <td>{{ $item->jns_kelas->nama_jenis }}</td>
                            <td>{{ $item->kuota_kelas }}</td>
                            <td class="text-center">
                                <h5><span class="badge {{ $item->status_kelas == 'Aktif' ? 'badge-success' : 'badge-danger' }}">{{ $item->status_kelas }}</span></h5>
                                @if ($list_kuota->firstWhere('kelas_id', $item->id) != null && $list_kuota->firstWhere('kelas_id', $item->id)->total >= $item->kuota_kelas)
                                <h5><span class="badge badge-danger">Kuota Penuh</span></h5>
                                @else
                                <h5><span class="badge badge-primary">Kuota Tersedia</span></h5>
                                @endif
                            </td>
                            <td>
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalEdit-{{ $item->id }}"><i class="fas fa-edit"></i> Edit</button>
                                @if($item->status_kelas == 'Aktif')
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalHapus-{{ $item->id }}"><i class="fas fa-trash"></i> Nonaktif</button>
                                @else
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAktif-{{ $item->id }}"><i class="fas fa-check"></i> Aktifkan</button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
        </div>
    </div>
</div>
<!-- /.row -->

<!-- Modal -->
<div class="modal fade" id="modalTambahJenis" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('kelas.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Jurusan Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Jenis</label>
                    <input type="text" name="nama_jenis" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" name="jenis_kelas" value="true" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($jenis_kelas as $jns)
<div class="modal fade" id="modalEditJenis-{{ $jns->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('kelas.update', $jns->id) }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Ubah Jurusan Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Jenis</label>
                    <input type="text" name="nama_jenis" class="form-control" value="{{ $jns->nama_jenis }}" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" name="jenis_kelas" value="true" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('kelas.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Kelas</label>
                    <input type="text" name="nama_kelas" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Jurusan Kelas</label>
                    <select name="jenis" class="form-control">
                        @foreach ($jenis_kelas as $item)
                        <option value="{{ $item->id }}" @if($loop->first) selected @endif>{{ $item->nama_jenis }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Kuota Kelas</label>
                    <input type="number" name="kuota_kelas" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" name="kelas" value="true" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($kelas as $kls)
<div class="modal fade" id="modalEdit-{{ $kls->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('kelas.update', $kls->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-header">
                <h5 class="modal-title">Form Ubah Data Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Kelas</label>
                    <input type="text" name="nama_kelas" class="form-control" value="{{ $kls->nama_kelas }}" />
                </div>
                <div class="form-group">
                    <label>Jurusan Kelas</label>
                    <select name="jenis" class="form-control">
                        @foreach ($jenis_kelas as $item)
                        <option {{ $item->id == $kls->jenis_id ? 'selected' : '' }} value="{{ $item->id }}" @if($loop->first) selected @endif>{{ $item->nama_jenis }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Kuota Kelas</label>
                    <input type="number" name="kuota_kelas" class="form-control" value="{{ $kls->kuota_kelas }}" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" name="kelas" value="true" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalHapus-{{ $kls->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('kelas.destroy', $kls->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-header">
                <h5 class="modal-title">Hapus Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Apakah mau menonaktifkan data kelas <strong>{{ $kls->nama_kelas }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" name="kelas" value="true" class="btn btn-primary">Ya</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalAktif-{{ $kls->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('kelas.aktifkan', $kls->id) }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Aktifkan Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Apakah mau mengaktifkan data kelas <strong>{{ $kls->nama_kelas }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" name="kelas" value="true" class="btn btn-primary">Ya</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection
