@extends('layouts.admin', ['halaman' => 'Konfigurasi Sistem'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
  });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        {{-- <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr class="text-center">
                                    <th>Parameter</th>
                                    <th>Value</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($konfigurasi as $item)
                                <form action="{{ route('konfigurasi.store') }}" method="POST">
                                @csrf
                                <tr>
                                    <td class="text-center">{{ $item->parameter }}</td>
                                    <td class="text-center"><input type="text" class="form-control" name="{{ $item->parameter }}" value="{{ $item->value }}" /></td>
                                    <td class="text-center">
                                        <button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i> Edit</button>
                                    </td>
                                </tr>
                                </form>
                                @endforeach
                            </tbody>
                        </table> --}}
                        <form action="{{ route('konfigurasi.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="">app_name</label>
                                <input type="text" class="form-control" name="app_name" value="{{ env('APP_NAME') }}" />
                            </div>
                            <div class="form-group">
                                <label class="">school_name</label>
                                <input type="text" class="form-control" name="school_name" value="{{ env('SCHOOL_NAME') }}" />
                            </div>
                            <div class="form-group">
                                <label class="">school_address</label>
                                <input type="text" class="form-control" name="school_address" value="{{ env('SCHOOL_ADDRESS') }}" />
                            </div>
                            <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-save"></i> Simpan</button>
                            </form>
                    </div>
                    <div class="col-lg-8 col-md-12">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalTambah">Tambah Persentase Penilaian</button>
                        <table class="table table-bordered table-striped mt-3">
                            <thead>
                                <tr class="text-center">
                                    <th>Semester</th>
                                    <th>Tugas</th>
                                    <th>Ulangan Harian</th>
                                    <th>UTS</th>
                                    <th>UAS</th>
                                    {{-- <th></th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($persentase as $item)
                                <tr>
                                    <td class="text-center">{{ $item->data_semester->periode }} - {{ $item->data_semester->semester }}</td>
                                    <td class="text-center">{{ $item->tugas }} %</td>
                                    <td class="text-center">{{ $item->harian }} %</td>
                                    <td class="text-center">{{ $item->uts }} %</td>
                                    <td class="text-center">{{ $item->uas }} %</td>
                                    {{-- <td class="text-center"><input type="text" value="{{ $item->tugas }}" name="tugas" /></td>
                                    <td class="text-center"><input type="text" value="{{ $item->harian }}" name="harian" /></td>
                                    <td class="text-center"><input type="text" value="{{ $item->uts }}" name="uts" /></td>
                                    <td class="text-center"><input type="text" value="{{ $item->uas }}" name="uas" /></td> --}}
                                    {{-- <td class="text-center">
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#" data-id="{{ $item->id }}"><i class="fas fa-edit"></i> Edit</button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#" data-id="{{ $item->id }}"><i class="fas fa-trash"></i> Hapus</button>
                                    </td> --}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('konfigurasi.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Semester</label>
                    <select name="semester" class="form-control">
                        <option value="" selected disabled>-- Pilih Semester --</option>
                        @foreach ($semester as $item)
                        <option value="{{ $item->id }}" {{ isset($request->semester) && $request->semester == $item->id ? 'selected' : '' }}>{{ $item->periode }} - {{ $item->semester }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nilai Ulangan Harian</label>
                            <input type="number" name="harian" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Nilai Tugas</label>
                            <input type="number" name="tugas" class="form-control" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nilai UTS</label>
                            <input type="number" name="uts" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Nilai UAS</label>
                            <input type="number" name="uas" class="form-control" />
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
