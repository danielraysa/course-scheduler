@extends('layouts.admin', ['halaman' => 'Konstrain Penjadwalan'])
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
            <form action="{{ route('konstrain.store') }}" method="POST">
            @csrf
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label>Max. Jam Berurutan</label>
                            <input type="text" name="max_jam_berurutan" value="{{ $konstrain->max_jam_berurutan }}" class="form-control" />
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label>Max. Guru Mengajar/Hari</label>
                            <input type="text" name="max_guru_mengajar" value="{{ $konstrain->max_guru_mengajar }}" class="form-control" />
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label>Max. Bobot Matpel/Hari</label>
                            <input type="text" name="max_bobot_matpel" value="{{ $konstrain->max_bobot_matpel }}" class="form-control" />
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label>Max. Iterasi</label>
                            <input type="text" name="max_iterasi" value="{{ $konstrain->max_iterasi }}" class="form-control" />
                        </div>
                    </div>
                </div>
                <hr>
                <h5><b>Max. Jam Matpel/Hari</b></h5>
                <div class="row">
                    @foreach ($konstrain_harian as $item)
                    @if ($loop->first || $loop->iteration == ($konstrain_harian->count()/2)+1)
                        <div class="col-lg-3">
                    @endif
                        <div class="form-group">
                            <label>{{ $item->hari }}</label>
                            <input type="hidden" class="form-control" name="id_hari[]" value="{{ $item->id }}" />
                            <input type="text" class="form-control" name="max_jam_matpel[]" value="{{ $item->max_jam_matpel }}" />
                        </div>
                    @if ($loop->last || $loop->iteration == $konstrain_harian->count()/2)
                    </div>
                    @endif
                    @endforeach
                </div>
                <button type="submit" class="btn btn-success">Simpan</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('semester.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Semester</label>
                    <input type="text" name="semester" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
