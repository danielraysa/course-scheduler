@extends('layouts.admin', ['halaman' => 'Data Guru'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $('.select2').select2();
    $("#example1").DataTable();
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
    $('.checkbox-user').change(function(){
        var value = $(this).is(':checked');
        if(value){
            $('.user-create-box').show();
        }else{
            $('.user-create-box').hide();
        }
    });
    $('.checkbox-user-edit').change(function(){
        var value = $(this).is(':checked');
        if(value){
            $('.user-edit-box').show();
        }else{
            $('.user-edit-box').hide();
        }
    });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-primary float-lg-left" data-toggle="modal" data-target="#modalTambah">
                    Tambah Data
                </button>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No.</th>
                            <th>NIP</th>
                            <th>Nama Guru</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($guru as $item)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="text-center">{{ $item->nip }}</td>
                            <td>{{ $item->nama_guru }}</td>
                            <td class="text-center"><h5><span class="badge {{ $item->status_guru == 'Aktif' ? 'badge-success' : 'badge-danger' }}">{{ $item->status_guru }}</span></h5></td>
                            {{-- <td class="text-center">{{ isset($item->kompetensi) ? $item->kompetensi->matpel_guru->nama_matpel : 'belum ada' }}</td> --}}
                            <td class="text-center">
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalEdit-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-edit"></i> Edit</button>
                                @if ($item->status_guru == 'Aktif')
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalHapus-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-trash"></i> Nonaktif</button>
                                @else
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAktif-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-check"></i> Aktifkan</button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('guru.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Data Guru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>NIP</label>
                    <input type="text" name="nip" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>Nama Guru</label>
                    <input type="text" name="nama_guru" class="form-control" required />
                </div>
                {{-- <div class="form-group">
                    <label>Mata Pelajaran Diampu</label>
                    <select name="matpel" class="form-control select2" style="width: 100%">
                        @foreach ($matpel as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_matpel }}</option>
                        @endforeach
                    </select>
                </div> --}}
                {{-- <div class="form-group">
                    <input type="checkbox" name="user_create" class="checkbox-user" value="true" /> Buat user untuk login
                </div> --}}
                <div class="user-create-box">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="username" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" required />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($guru as $item)
<div class="modal fade" id="modalEdit-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('guru.update', $item) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-header">
                <h5 class="modal-title">Form Ubah Data Guru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>NIP</label>
                    <input type="text" name="nip" value="{{ $item->nip }}" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Nama Guru</label>
                    <input type="text" name="nama_guru" value="{{ $item->nama_guru }}" class="form-control" />
                </div>
                {{-- <div class="form-group">
                    <label>Mata Pelajaran Diampu</label>
                    <select name="matpel" class="form-control select2" style="width: 100%">
                        @foreach ($matpel as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_matpel }}</option>
                        @endforeach
                    </select>
                </div> --}}
                @if($item->data_user != null)
                <div class="form-group">
                    <input type="checkbox" name="user_edit" class="checkbox-user-edit" value="true" /> Edit user login
                </div>
                <div class="user-edit-box" style="display: none">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" readonly value="{{ $item->data_user->email }}" />
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Isi untuk mengubah password"  />
                    </div>
                </div>
                @else
                {{-- <div class="form-group">
                    <input type="checkbox" name="user_create" class="checkbox-user" value="true" /> Buat user untuk login
                </div> --}}
                <div class="user-create-box">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" />
                    </div>
                </div>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalHapus-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('guru.destroy', $item) }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-header">
                <h5 class="modal-title">Nonaktifkan Data Guru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Apakah mau menonaktifkan data guru <strong>{{ $item->nama_guru }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary">Ya</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalAktif-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('guru.aktifkan', $item) }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Aktifkan Data Guru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Apakah mau mengaktifkan data guru <strong>{{ $item->nama_guru }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary">Ya</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection
