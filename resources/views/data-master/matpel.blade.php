@extends('layouts.admin', ['halaman' => 'Data Mata Pelajaran'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
  });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-primary float-lg-left" data-toggle="modal" data-target="#modalTambah">
                    Tambah Data
                </button>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No.</th>
                            <th>Nama Mata Pelajaran</th>
                            <th>Nilai KKM</th>
                            <th>Bobot</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($matpel as $item)
                        <tr class="text-center">
                            <td class="align-middle">{{ $loop->iteration }}</td>
                            <td class="align-middle">{{ $item->nama_matpel }}</td>
                            <td class="align-middle">{{ $item->nilai_kkm }}</td>
                            <td class="align-middle">{{ $item->bobot_matpel }}</td>
                            <td class="text-center"><h5><span class="badge {{ $item->status_matpel == 'Aktif' ? 'badge-success' : 'badge-danger' }}">{{ $item->status_matpel }}</span></h5></td>
                            <td>
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modalEdit-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-edit"></i> Edit</button>
                                @if ($item->status_matpel == 'Aktif')
                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modalHapus-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-trash"></i> Nonaktif</button>
                                @else
                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modalAktif-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-check"></i> Aktifkan</button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

<!-- Modal -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('mata-pelajaran.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Data Mata Pelajaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Mata Pelajaran</label>
                    <input type="text" name="nama_matpel" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Jenis</label>
                    <select name="jenis" class="form-control">
                        <option value="umum">Umum</option>
                        <option value="keagamaan">Keagamaan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Nilai KKM</label>
                    <input type="number" min="1" max="100" name="nilai_kkm" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Bobot Mata Pelajaran</label>
                    <input type="number" value="1" min="1" name="bobot" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach($matpel as $item)
<div class="modal fade" id="modalEdit-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('mata-pelajaran.update', $item) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-header">
                <h5 class="modal-title">Form Ubah Data Mata Pelajaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Mata Pelajaran</label>
                    <input type="text" name="nama_matpel" value="{{ $item->nama_matpel }}" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Jenis</label>
                    <select name="jenis" class="form-control">
                        <option {{ $item->jenis == 'umum' ? 'selected' : '' }} value="umum">Umum</option>
                        <option {{ $item->jenis == 'keagamaan' ? 'selected' : '' }} value="keagamaan">Keagamaan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Nilai KKM</label>
                    <input type="number" value="{{ $item->nilai_kkm }}" min="1" max="100" name="nilai_kkm" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Bobot Mata Pelajaran</label>
                    <input type="number" value="{{ $item->bobot_matpel }}" min="1" name="bobot" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalAktif-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('mata-pelajaran.aktifkan', $item) }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Aktifkan Mata Pelajaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Apakah mau mengaktifkan mata pelajaran <strong>{{ $item->nama_matpel }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Ya</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalHapus-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('mata-pelajaran.destroy', $item) }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-header">
                <h5 class="modal-title">Hapus Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Apakah mau menonaktifkan mata pelajaran <strong>{{ $item->nama_matpel }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary">Ya</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection
