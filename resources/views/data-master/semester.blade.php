@extends('layouts.admin', ['halaman' => 'Data Semester/Tahun Ajaran'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
  });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-primary float-lg-left" data-toggle="modal" data-target="#modalTambah">
                    Tambah Data
                </button>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No.</th>
                            <th>Semester/Periode</th>
                            <th>Harian</th>
                            <th>Tugas</th>
                            <th>Uts</th>
                            <th>Uas</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($semester as $item)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="text-center">{{ $item->periode }} - {{ $item->semester }} ({{ $item->semester == 1 ? 'Ganjil' : 'Genap' }})</td>
                            <td class="text-center">{{ $item->persentase_nilai->harian }}%</td>
                            <td class="text-center">{{ $item->persentase_nilai->tugas }}%</td>
                            <td class="text-center">{{ $item->persentase_nilai->uts }}%</td>
                            <td class="text-center">{{ $item->persentase_nilai->uas }}%</td>
                            <td class="text-center"><h5><span class="badge {{ $item->status == App\Models\Semester::AKTIF ? 'badge-success' : 'badge-danger' }}">{{ $item->status == App\Models\Semester::AKTIF ? 'Aktif' : 'Tidak Aktif' }}</span></h5></td>
                            <td class="text-center">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalEdit-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-edit"></i> Edit</button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalHapus-{{ $item->id }}" data-id="{{ $item->id }}"><i class="fas fa-trash"></i> Hapus</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('semester.store') }}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Data Semester</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Periode</label>
                    <input type="text" name="periode" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>Semester</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="semester" value="1"/>
                        <label class="form-check-label" for="flexCheckDefault">
                          1 (Gasal)
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="semester" value="2"/>
                        <label class="form-check-label" for="flexCheckDefault">
                            2 (Genap)
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>% Ulangan Harian</label>
                    <input type="number" name="harian" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>% Tugas</label>
                    <input type="number" name="tugas" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>% UTS</label>
                    <input type="number" name="uts" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>% UAS</label>
                    <input type="number" name="uas" class="form-control" required />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach($semester as $item)
<div class="modal fade" id="modalEdit-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('semester.update', $item) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-header">
                <h5 class="modal-title">Form Ubah Data Semester</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Periode</label>
                    <input type="text" name="periode" value="{{ $item->periode }}" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Semester</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="semester" value="1" @if($item->semester == 1) checked @endif/>
                        <label class="form-check-label" for="flexCheckDefault">
                          1 (Gasal)
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="semester" value="2" @if($item->semester == 2) checked @endif/>
                        <label class="form-check-label" for="flexCheckDefault">
                            2 (Genap)
                        </label>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-6">
                        <label>% Ulangan Harian</label>
                        <input type="number" name="harian" class="form-control" value="{{ $item->persentase_nilai->harian }}" />
                    </div>
                    <div class="col-6">
                        <label>% Tugas</label>
                        <input type="number" name="tugas" class="form-control" value="{{ $item->persentase_nilai->tugas }}" />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-6">
                        <label>% UTS</label>
                        <input type="number" name="uts" class="form-control" value="{{ $item->persentase_nilai->uts }}" />
                    </div>
                    <div class="col-6">
                        <label>% UAS</label>
                        <input type="number" name="uas" class="form-control" value="{{ $item->persentase_nilai->uas }}" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalHapus-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="{{ route('semester.destroy', $item) }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-header">
                <h5 class="modal-title">Hapus Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Apakah mau menghapus semester <strong>{{ $item->periode }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary">Ya</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection
