@extends('layouts.admin', ['halaman' => 'Jadwal Pelajaran Hari Ini'])
@section('content')
{{-- <div class="row mb-3">
    <div class="col-3">
        <div class="card bg-light">
            <div class="card-body">
                <p class="mb-0">Kehadiran</p>
                <h3 class="mb-0">{{ $presensi_hadir }}</h3>
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="card bg-light">
            <div class="card-body">
                <p class="mb-0">Alpha</p>
                <h3 class="mb-0">{{ $presensi_alpha }}</h3>
            </div>
        </div>
    </div>
</div> --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            {{-- <th>No.</th> --}}
                            <th>Hari</th>
                            <th>Jam Ke</th>
                            <th>Mata Pelajaran</th>
                            <th>Guru</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jadwal as $item)
                        <tr>
                            {{-- <td>{{ $loop->iteration }}</td> --}}
                            <td>{{ $item->hari }}</td>
                            <td>{{ $item->jam_matpel }}</td>
                            <td>{{ $item->data_matpel->nama_matpel }}</td>
                            <td>{{ $item->data_guru->nama_guru }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
@endsection
