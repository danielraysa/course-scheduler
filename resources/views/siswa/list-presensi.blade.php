@extends('layouts.admin', ['halaman' => 'Detail Presensi Tanggal '.$tanggal])
@push('js')

@endpush
@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Hari</th>
                            <th>Jam Ke</th>
                            <th>Mata Pelajaran</th>
                            <th>Guru</th>
                            <th>Status</th>
                        </tr>
                        @foreach ($cek_presensi as $item)    
                        <tr>
                            <td>{{ $item->data_jadwal->hari }}</td>
                            <td>{{ $item->data_jadwal->jam_matpel }}</td>
                            <td>{{ $item->data_matpel->nama_matpel }}</td>
                            <td>{{ $item->data_jadwal->data_guru->nama_guru }}</td>
                            <td>{{ $item->status_kehadiran() }}</td>
                        </tr>
                        @endforeach
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
