@extends('layouts.admin', ['halaman' => 'Presensi Bulan Ini'])
@push('js')
<script src="{{ asset('plugins/chart.js/Chart.bundle.min.js') }}"></script>
<script>
  $(function () {
    var dataChart = {!! json_encode($data_chart) !!}
    var ctx = document.getElementById('myChart').getContext('2d')
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                label: "Presensi",
                data: dataChart.data,
                backgroundColor: dataChart.backgroundColor
            }],
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: dataChart.labels
        }
    })
  })
</script>
@endpush
@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
            <div class="col-md-3">
                <div class="card bg-primary">
                    <div class="card-body">
                        <h4 class="card-title">Kehadiran</h4>
                        <h3 class="card-text font-weight-bold">{{ round(100 * $presensi_hadir/$presensi_total, 2) }} %</h3>
                    </div>
                </div>
                <div class="card bg-danger">
                    <div class="card-body">
                        <h4 class="card-title">Alpha</h4>
                        <h3 class="card-text font-weight-bold">{{ round(100 * $presensi_alpha/$presensi_total, 2) }} %</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-info">
                    <div class="card-body">
                        <h4 class="card-title">Sakit</h4>
                        <h3 class="card-text font-weight-bold">{{ round(100 * $presensi_sakit/$presensi_total, 2) }} %</h3>
                    </div>
                </div>
                <div class="card bg-warning">
                    <div class="card-body">
                        <h4 class="card-title">Ijin</h4>
                        <h3 class="card-text font-weight-bold">{{ round(100 * $presensi_ijin/$presensi_total, 2) }} %</h3>
                    </div>
                </div>
                <form action="{{ route('presensi.detail') }}">
                    <div class="form-group">
                        <label>Lihat/Cek Presensi</label>
                        <input type="date" class="form-control" name="tanggal" />
                        <button type="submit" class="btn btn-primary">Lihat</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
