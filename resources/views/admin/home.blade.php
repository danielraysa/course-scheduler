@extends('layouts.admin', ['halaman' => 'Halaman Utama'])
@push('js')
<script src="{{ asset('plugins/chart.js/Chart.bundle.min.js') }}"></script>
<script>
  $(function () {
    var ctx = document.getElementById('myChart').getContext('2d')
    var ctxSiswa = document.getElementById('myChartSiswa').getContext('2d')
    var option = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Aktif', 'Tidak Aktif'],
            datasets: [{
                label: 'Jumlah',
                // data: [{{ $data_guru->count() }}, {{ $data_siswa->count() }},{{ $data_kelas->count() }}],
                data: [{{ $data_guru->where('status_guru', 'Aktif')->count() }}, {{ $data_guru->where('status_guru', 'Tidak Aktif')->count() }}],
                backgroundColor: [
                    // 'rgba(102, 0, 0, 0.8)',
                    'rgba(0, 0, 153, 0.8)',
                    'rgba(153, 51, 0, 0.8)'
                ],
                borderColor: [
                    // 'rgba(102, 0, 0, 1)',
                    'rgba(0, 0, 153, 1)',
                    'rgba(153, 51, 0, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: option
    })
    var myChartSiswa = new Chart(ctxSiswa, {
        type: 'bar',
        data: {
            labels: ['Aktif', 'Tidak Aktif'],
            datasets: [{
                label: 'Jumlah',
                data: [{{ $data_siswa->where('status_siswa', 'Aktif')->count() }}, {{ $data_siswa->where('status_siswa', 'Tidak Aktif')->count() }}],
                backgroundColor: [
                    'rgba(0, 0, 153, 0.8)',
                    'rgba(153, 51, 0, 0.8)'
                ],
                borderColor: [
                    'rgba(0, 0, 153, 1)',
                    'rgba(153, 51, 0, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: option
    })
  })
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Jumlah Guru Aktif</h4>
                <h3 class="card-text font-weight-bold">{{ $data_guru->where('status_guru', 'Aktif')->count() }}</h3>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Jumlah Siswa Aktif</h4>
                <h3 class="card-text font-weight-bold">{{ $data_siswa->where('status_siswa', 'Aktif')->count() }}</h3>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Jumlah Kelas</h4>
                <h3 class="card-text font-weight-bold">{{ $data_kelas->count() }}</h3>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6">
                <h5>Grafik Batang Data Guru</h5>
                <canvas id="myChart" height="200"></canvas>
            </div>
            <div class="col-lg-6">
                <h5>Grafik Batang Data Siswa</h5>
                <canvas id="myChartSiswa" height="200"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection
