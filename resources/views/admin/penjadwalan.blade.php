@extends('layouts.admin', ['halaman' => 'Penjadwalan Mata Pelajaran'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
  });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body table-responsive">
                <table class="table table-bordered">
                    <tr class="text-center">
                        <th rowspan="2">Kelas</th>
                        <th rowspan="2">Hari</th>
                        <th colspan="{{ $konstrain->max_jam_matpel }}">Jam</th>
                    </tr>
                    <tr class="text-center">
                    @for($i = 0; $i < $konstrain->max_jam_matpel; $i++)
                        <th>{{ $i+1 }}</th>
                    @endfor
                    </tr>
                    @foreach ($kelas as $kls)
                        <tr>
                            <td rowspan="{{ $harian->count() }}">{{ $kls->nama_kelas }}</td>
                            @foreach ($harian as $hr)
                            <td>{{ $hr->hari }}</td>
                            @for($i = 0; $i < $konstrain->max_jam_matpel; $i++)
                            <td>
                                <select class="form-control">
                                    @foreach ($guru as $gr)
                                        <option value="{{ $gr->id }}">{{ $gr->nama_guru }}</option>
                                    @endforeach
                                </select>
                            </td>
                            @endfor
                        </tr>
                        <tr>
                            @endforeach
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
