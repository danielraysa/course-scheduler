@extends('layouts.admin', ['halaman' => 'Penilaian'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
    $('#kelas').change(function(){
        var id = $(this).val();
        var data = $(this).find('option:selected').text();
        $('#title-page').text(`Penilaian Kelas ${data}`);
        $.ajax({
            url:"{{ route('penilaian.get-siswa') }}",
            type: "GET",
            data: {kelas: id},
            success: function(result){
                $('#list-table').html(result);
            },
            error: function(){
                $('#list-table').html("<p>Error</p>");
            }
        });
    });
  });
</script>
@endpush
@push('menu-button')
<div class="col-sm-6">
<div class="d-flex justify-content-end">
    <a href="?lihat=true" class="btn btn-primary mr-2">Lihat Nilai</a>
    <a href="?ubah=true" class="btn btn-warning">Ubah Nilai</a>
</div>
</div>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('penilaian.store') }}">
                @csrf
                <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label>Kelas</label>
                        <select name="kelas" id="kelas" class="form-control select2">
                            <option value="" selected disabled>-- Pilih Kelas --</option>
                            @foreach ($kelas as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label>Jenis Nilai</label>
                        <select name="jenis_nilai" class="form-control select2">
                            <option value="" selected disabled>-- Pilih Jenis Nilai --</option>
                            <option value="tugas">Tugas</option>
                            <option value="harian">Harian</option>
                            <option value="uts">UTS</option>
                            <option value="uas">UAS</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label>Mata Pelajaran</label>
                        <select name="matpel" class="form-control select2">
                            <option value="" selected disabled>-- Pilih Matpel --</option>
                            @foreach ($matpel as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_matpel }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                </div>
                <div id="list-table">
                </div>
                {{-- @if(isset($request->kelas))
                <table class="table">
                    <tr>
                        <th>No.</th>
                        <th>Nama Siswa</th>
                        <th>Asrama</th>
                        <th>Kehadiran</th>
                    </tr>
                    @php
                    $i = 1;
                    @endphp
                    @foreach ($siswa as $item)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $item->nama_siswa }}</td>
                            <td>Asrama</td>
                            <td><input name="kehadiran[]" value="{{ $item->id }}"/></td>
                        </tr>
                    @endforeach
                </table>
                <button class="btn btn-success" type="submit" name="simpan">Simpan</button>
                @endif --}}
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
