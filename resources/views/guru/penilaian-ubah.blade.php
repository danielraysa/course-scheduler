@extends('layouts.admin', ['halaman' => 'Ubah Nilai '.(isset($data_kelas) ? 'Kelas '.$data_kelas->nama_kelas : '')])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
    $('#kelas, #matpel').change(function(){
        let id_kelas = $('#kelas').val();
        let id_matpel = $('#matpel').val();
        var kelas = $('#kelas').find('option:selected').text();
        $('#title-page').text(`Ubah Nilai Kelas ${kelas}`);
        if (id_kelas != null && id_matpel != null) {
            $('#form_ubah').submit();
        }
        /* $.ajax({
            url:"{{ route('penilaian.get-siswa') }}",
            type: "GET",
            data: {kelas: id},
            success: function(result){
                $('#list-table').html(result);
            },
            error: function(){
                $('#list-table').html("<p>Error</p>");
            }
        }); */
    });
    $('#jenis_nilai').change(function(){
        $('#form_ubah').submit();
    });
  });
</script>
@endpush

@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <form id="form_ubah" action="" method="GET">
                    <input type="hidden" name="ubah" value="true" /> 
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Kelas</label>
                            <select name="kelas" id="kelas" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Kelas --</option>
                                @foreach ($kelas as $item)
                                <option value="{{ $item->id }}" {{ isset($request) && $request->kelas == $item->id ? 'selected' : '' }}>{{ $item->nama_kelas }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Mata Pelajaran</label>
                            <select name="matpel" id="matpel" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Matpel --</option>
                                @foreach ($matpel as $item)
                                <option value="{{ $item->id }}" {{ isset($request) && $request->matpel == $item->id ? 'selected' : '' }}>{{ $item->nama_matpel }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if(isset($nilai))
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Pilih Nilai</label>
                            <select name="jenis_nilai" id="jenis_nilai" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Nilai --</option>
                                @foreach ($nilai as $item)
                                <option value="{{ $item->kategori.'_'.$item->tanggal }}" {{ $item->kategori.'_'.$item->tanggal == request()->get('jenis_nilai') ? 'selected' : '' }}>{{ ucwords($item->kategori) }} ({{ $item->tanggal }})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                </div>
                </form>
                <hr>
                <div id="list-table">
                    @if (isset($data_nilai))
                    <form action="{{ route('penilaian.update_nilai') }}" method="POST">
                    @csrf
                    <table class="table table-bordered">
                        <tr>
                            <th>No.</th>
                            <th>NIS</th>
                            <th>Nama Siswa</th>
                            <th>Nilai</th>
                        </tr>
                        @php
                        $i = 1;   
                        @endphp
                        @foreach ($data_nilai as $item)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $item->data_siswa->nis }}</td>
                                <td>{{ $item->data_siswa->nama_siswa }}</td>
                                <td>
                                    <input type="hidden" name="id_nilai[]" value="{{ $item->id }}" readonly/>
                                    <input type="number" name="nilai_siswa[]" value="{{ $item->nilai }}" class="form-control" />
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <button type="submit" class="btn btn-success">Update</button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
