@extends('layouts.admin', ['halaman' => 'Lihat Nilai'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
    $('#kelas, #matpel').change(function(){
        let id_kelas = $('#kelas').val();
        let id_matpel = $('#matpel').val();
        if (id_kelas != null && id_matpel != null) {
            $('#form_ubah').submit();
        }
        /* $.ajax({
            url:"{{ route('penilaian.get-siswa') }}",
            type: "GET",
            data: {kelas: id},
            success: function(result){
                $('#list-table').html(result);
            },
            error: function(){
                $('#list-table').html("<p>Error</p>");
            }
        }); */
    });
    $('#jenis_nilai').change(function(){
        $('#form_ubah').submit();
    });
  });
</script>
@endpush

@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <form id="form_ubah" action="" method="GET">
                    <input type="hidden" name="lihat" value="true" /> 
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Kelas</label>
                            <select name="kelas" id="kelas" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Kelas --</option>
                                @foreach ($kelas as $item)
                                <option value="{{ $item->id }}" {{ isset($request) && $request->kelas == $item->id ? 'selected' : '' }}>{{ $item->nama_kelas }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Mata Pelajaran</label>
                            <select name="matpel" id="matpel" class="form-control select2">
                                <option value="" selected disabled>-- Pilih Matpel --</option>
                                @foreach ($matpel as $item)
                                <option value="{{ $item->id }}" {{ isset($request) && $request->matpel == $item->id ? 'selected' : '' }}>{{ $item->nama_matpel }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                </form>
                <hr>
                <div id="list-table">
                    @if (isset($siswa))
                    <table class="table table-bordered">
                        <tr>
                            <th>No.</th>
                            <th>Nama Siswa</th>
                            <th class="text-center">Tugas</th>
                            <th class="text-center">Harian</th>
                            <th class="text-center">UTS</th>
                            <th class="text-center">UAS</th>
                            <th class="text-center">Nilai Akhir</th>
                            <th class="text-center">Status</th>
                        </tr>
                        @php
                        $i = 1;   
                        @endphp
                        @foreach ($siswa as $item)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $item->nama_siswa }}</td>
                                @php
                                    $nilai = App\Helper\KalkulasiNilai::averageNilai($item, $persentase);
                                @endphp
                                <td class="text-center">{{ $nilai['rata_tugas'] }}</td>
                                <td class="text-center">{{ $nilai['rata_harian'] }}</td>
                                <td class="text-center">{{ $nilai['rata_uts'] }}</td>
                                <td class="text-center">{{ $nilai['rata_uas'] }}</td>
                                <td class="text-center">{{ $nilai['nilai_akhir'] }}</td>
                                @php
                                    $badge = $nilai['nilai_akhir'] > 75 ? 'badge-success' : 'badge-danger';
                                    $status = $nilai['nilai_akhir'] > 75 ? 'Lulus' : 'Remidi';
                                @endphp
                                <td class="text-center"><span class="badge {{ $badge}}" style="font-size: 1rem !important">{{ $status }}</span></td>
                            </tr>
                        @endforeach
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
