@extends('layouts.admin', ['halaman' => 'Jadwal Mengajar Hari Ini'])
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Hari</th>
                            <th>Jam Ke</th>
                            <th>Kelas</th>
                            <th>Mata Pelajaran</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jadwal as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->hari }}</td>
                            <td>{{ $item->jam_matpel }}</td>
                            <td>{{ $item->data_kelas->nama_kelas }}</td>
                            <td>{{ $item->data_matpel->nama_matpel }}</td>
                        </tr>
                        @endforeach
                        @if($jadwal->count() == 0)
                        <tr>
                            <td class="text-center" colspan="5">Tidak ada jadwal hari ini</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
@endsection
