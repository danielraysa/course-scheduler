<div class="row mb-3">
    <div class="col-lg-3"><strong>Total Hadir:</strong> <span id="total-hadir"></span></div>
    <div class="col-lg-3"><strong>Total Sakit:</strong> <span id="total-sakit"></span></div>
    <div class="col-lg-3"><strong>Total Ijin:</strong> <span id="total-ijin"></span></div>
    <div class="col-lg-3"><strong>Total Alpha:</strong> <span id="total-alpha"></span></div>
</div>
<table class="table table-bordered">
    <tr>
        <th>No.</th>
        <th>NIS</th>
        <th>Nama Siswa</th>
        <th>Kehadiran</th>
    </tr>
    @foreach ($penempatan as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->data_siswa->nis }}</td>
            <td>{{ $item->data_siswa->nama_siswa }}</td>
            <td>
                {{-- <input type="checkbox" name="kehadiran[]" value="{{ $item->data_siswa->id }}"/> --}}
                <input type="hidden" name="id_siswa[]" value="{{ $item->data_siswa->id }}"/>
                <select name="kehadiran[]" class="form-control select-hadir">
                    <option value="H">Hadir</option>
                    <option value="I">Ijin</option>
                    <option value="S">Sakit</option>
                    <option value="A">Alpha</option>
                </select>
            </td>
        </tr>
    @endforeach
</table>
<input type="hidden" name="kelas" value="{{ $request->kelas }}"/>
<button class="btn btn-success" type="submit" name="simpan">Simpan</button>
<script>
    var jumlah_siswa = {{ $penempatan->count() }}
    var jumlah_alpha = 0
    $('#total-hadir').text(jumlah_siswa)
    $('#total-sakit').text(jumlah_alpha)
    $('#total-ijin').text(jumlah_alpha)
    $('#total-alpha').text(jumlah_alpha)
    $('.select-hadir').change(function() {
        var value_hadir = $('.select-hadir').find('[value="H"]:selected').length;
        var value_sakit = $('.select-hadir').find('[value="S"]:selected').length;
        var value_ijin = $('.select-hadir').find('[value="I"]:selected').length;
        var value_alpha = $('.select-hadir').find('[value="A"]:selected').length;
        $('#total-hadir').text(value_hadir)
        $('#total-sakit').text(value_sakit)
        $('#total-ijin').text(value_ijin)
        $('#total-alpha').text(value_alpha)
    })
</script>
