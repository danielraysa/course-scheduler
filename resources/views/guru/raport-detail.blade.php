@extends('layouts.admin', ['halaman' => 'Rapor Siswa '.$data_siswa->nama_siswa])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({

        });
        $('#example2').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            // "autoWidth": false,
            // "responsive": true,
        });
    });
    $(".alert").alert();
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-3 col-6">
                            <div class="card bg-light">
                                <div class="card-body">
                                    <p class="mb-0">Kehadiran</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $presensi_hadir }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="card bg-danger">
                                <div class="card-body">
                                    <p class="mb-0">Alpha</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $presensi_alpha }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="card bg-secondary">
                                <div class="card-body">
                                    <p class="mb-0">Sakit</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $presensi_sakit }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="card bg-warning">
                                <div class="card-body">
                                    <p class="mb-0">Ijin</p>
                                    <h3 class="mb-0 font-weight-bold">{{ $presensi_ijin }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('rapor.cetak', $id) }}" target="_blank" class="btn btn-primary mb-2">Cetak Rapor</a>

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr class="text-center">
                                <th>No.</th>
                                <th>Mata Pelajaran</th>
                                <th>Tugas</th>
                                <th>Harian</th>
                                <th>UTS</th>
                                <th>UAS</th>
                                <th>Nilai Akhir</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($matpel as $item)
                            <tr class="text-center">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nama_matpel }}</td>
                                @php
                                    $nilai = App\Helper\KalkulasiNilai::averageNilai($item, $persentase);
                                @endphp

                                <td>{{ $nilai['rata_tugas'] }}</td>
                                <td>{{ $nilai['rata_harian'] }}</td>
                                <td>{{ $nilai['rata_uts'] }}</td>
                                <td>{{ $nilai['rata_uas'] }}</td>
                                <td>{{ $nilai['nilai_akhir'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
