@extends('layouts.admin', ['halaman' => 'Presensi Susulan '.(isset($kelas_terpilih) ? 'Kelas '.$kelas_terpilih->nama_kelas : '')])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    /* $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    }); */
    $('#jadwal').change(function(){
        var id = $(this).val();
        $.ajax({
            url:"{{ route('presensi.get-siswa') }}",
            type: "GET",
            data: {jadwal: id},
            success: function(result){
                $('#list-table').html(result);
            },
            error: function(err){
                $('#list-table').html(`<p>Error: ${err.toString()} </p>`);
            }
        });
    });
    $('#kelas').change(function(){
        var id = $(this).val();
        $.ajax({
            url:"{{ route('presensi.get-siswa') }}",
            type: "GET",
            data: {kelas: id},
            success: function(result){
                $('#list-table').html(result);
            },
            error: function(err){
                $('#list-table').html("<p>Error</p>");
            }
        });
    });
  });
</script>
@endpush
@push('menu-button')
<div class="col-sm-6">
<div class="d-flex justify-content-end">
    <a href="{{ route('presensi.list') }}" class="btn btn-primary mr-2">Lihat Presensi</a>
    <a href="{{ route('presensi.susulan') }}" class="btn btn-danger">Presensi Susulan/Ubah Presensi</a>
</div>
</div>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <form class="mb-3" action="">
                <div class="row">
                {{-- @if(isset($jadwal)) --}}
                <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal" value="{{ request()->get('tanggal') }}" class="form-control" onchange="this.form.submit()" />
                    </div>
                </div>
                @if(isset($jadwal))
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label>Jadwal</label>
                        <select name="jadwal" id="jadwal" class="form-control select2" onchange="this.form.submit()">
                            <option value="" selected disabled>-- Pilih Jadwal --</option>
                            @foreach ($jadwal as $item)
                            <option value="{{ $item->id }}" {{ isset($request->jadwal) && $item->id == $request->jadwal ? 'selected' : '' }}>Jam {{ $item->jam_matpel }} -- {{ $item->data_kelas->nama_kelas }} ({{ $item->data_matpel->nama_matpel }})</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                {{-- @else --}}
                {{-- <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label>Kelas</label>
                        <select name="kelas" id="kelas" class="form-control select2" required>
                            <option value="" selected disabled>-- Pilih Kelas --</option>
                            @foreach ($kelas as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label>Mata Pelajaran</label>
                        <select name="matpel" class="form-control select2" required>
                            <option value="" selected disabled>-- Pilih Matpel --</option>
                            @foreach ($matpel as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_matpel }}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}
                @endif
            </div>
            {{-- <button type="submit" class="btn btn-sm btn-success">Cari</button> --}}
            </form>
            <div class="row mb-3">
                <div class="col-lg-3"><strong>Total Hadir:</strong> <span id="total-hadir"></span></div>
                <div class="col-lg-3"><strong>Total Sakit:</strong> <span id="total-sakit"></span></div>
                <div class="col-lg-3"><strong>Total Ijin:</strong> <span id="total-ijin"></span></div>
                <div class="col-lg-3"><strong>Total Alpha:</strong> <span id="total-alpha"></span></div>
            </div>
                @if(isset($list_presensi) || isset($data_siswa))
                <form action="{{ route('presensi.simpan-susulan') }}" method="POST">
                @csrf
                <table id="example1" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Kehadiran</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($list_presensi))
                    @foreach ($list_presensi as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->data_siswa->nis }}</td>
                        <td>{{ $item->data_siswa->nama_siswa }}</td>
                        <td>
                            {{-- <input type="checkbox" name="kehadiran[]" value="{{ $item->data_siswa->id }}"/> --}}
                            <input type="hidden" name="id_siswa[]" value="{{ $item->data_siswa->id }}"/>
                            <select name="kehadiran[]" class="form-control select-hadir">
                                <option value="H" {{ $item->status == 'H' ? 'selected' : '' }}>Hadir</option>
                                <option value="I" {{ $item->status == 'I' ? 'selected' : '' }}>Ijin</option>
                                <option value="S" {{ $item->status == 'S' ? 'selected' : '' }}>Sakit</option>
                                <option value="A" {{ $item->status == 'A' ? 'selected' : '' }}>Alpha</option>
                            </select>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    @if(isset($data_siswa))
                    @foreach ($data_siswa as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->data_siswa->nama_siswa }}</td>
                        <td>
                            <input type="hidden" name="id_siswa[]" value="{{ $item->data_siswa->id }}"/>
                            <select name="kehadiran[]" class="form-control select-hadir">
                                <option value="H">Hadir</option>
                                <option value="I">Ijin</option>
                                <option value="S">Sakit</option>
                                <option value="A">Alpha</option>
                            </select>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
                <input type="hidden" name="jadwal" value="{{ request()->get('jadwal') }}" />
                <input type="hidden" name="tanggal" value="{{ request()->get('tanggal') }}" />
                <button class="btn btn-success" type="submit" name="simpan">Simpan</button>
                </form>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
@endsection

@push('js')
<script>
    var value_hadir = $('.select-hadir').find('[value="H"]:selected').length;
    var value_sakit = $('.select-hadir').find('[value="S"]:selected').length;
    var value_ijin = $('.select-hadir').find('[value="I"]:selected').length;
    var value_alpha = $('.select-hadir').find('[value="A"]:selected').length;
    $('#total-hadir').text(value_hadir)
    $('#total-sakit').text(value_sakit)
    $('#total-ijin').text(value_ijin)
    $('#total-alpha').text(value_alpha)
    $('.select-hadir').change(function() {
        value_hadir = $('.select-hadir').find('[value="H"]:selected').length;
        value_sakit = $('.select-hadir').find('[value="S"]:selected').length;
        value_ijin = $('.select-hadir').find('[value="I"]:selected').length;
        value_alpha = $('.select-hadir').find('[value="A"]:selected').length;
        $('#total-hadir').text(value_hadir)
        $('#total-sakit').text(value_sakit)
        $('#total-ijin').text(value_ijin)
        $('#total-alpha').text(value_alpha)
    })
</script>
@endpush

