@extends('layouts.admin', ['halaman' => 'Presensi'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });

  });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <form action="" method="GET">
                <div class="row mb-3">
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="date" class="form-control" name="tanggal" value="{{ isset($request->tanggal) ? $request->tanggal : '' }}" required/>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label>Kelas</label>
                            <select name="kelas" id="kelas" class="form-control select2" required>
                                <option value="" selected disabled>-- Pilih Kelas --</option>
                                @foreach ($kelas as $item)
                                <option value="{{ $item->id }}" {{ isset($request->kelas) && $request->kelas == $item->id ? 'selected' : '' }}>{{ $item->nama_kelas }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                            <label>Mata Pelajaran</label>
                            <select name="matpel" class="form-control select2" required>
                                <option value="" selected disabled>-- Pilih Matpel --</option>
                                @foreach ($matpel as $item)
                                <option value="{{ $item->id }}" {{ isset($request->matpel) && $request->matpel == $item->id ? 'selected' : '' }}>{{ $item->nama_matpel }} ({{ $item->bobot_matpel }} sks)</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-12">
                        <button class="btn btn-primary btn-block" style="margin-top: 1.9rem" type="submit">Cari</button>
                    </div>
                </form>
                </div>
                {{-- <div id="list-table">
                </div> --}}
                @if(isset($list_presensi))
                <table id="example1" class="table">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Kehadiran</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($list_presensi as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->data_siswa->nis }}</td>
                            <td>{{ $item->data_siswa->nama_siswa }}</td>
                            <td class="text-center">{{ $item->status_kehadiran() }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
