@extends('layouts.admin', ['halaman' => 'Presensi Siswa'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    /* $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    }); */
    $('#jadwal').change(function(){
        var id = $(this).val();
        var data = $(this).find('option:selected').data('kelas');
        $('#title-page').text($('#title-page').text() + " - " + data);
        $.ajax({
            url:"{{ route('presensi.get-siswa') }}",
            type: "GET",
            data: {jadwal: id},
            success: function(result){
                $('#list-table').html(result);
            },
            error: function(err){
                $('#list-table').html(`<p>Error: ${err.toString()} </p>`);
            }
        });
    });
    $('#kelas').change(function(){
        var id = $(this).val();
        $.ajax({
            url:"{{ route('presensi.get-siswa') }}",
            type: "GET",
            data: {kelas: id},
            success: function(result){
                $('#list-table').html(result);
            },
            error: function(err){
                $('#list-table').html(`<p>Error: ${err.toString()}</p>`);
            }
        });
    });
  });
</script>
@endpush
@push('menu-button')
<div class="col-sm-6">
<div class="d-flex justify-content-end">
    <a href="{{ route('presensi.list') }}" class="btn btn-primary mr-2">Lihat Presensi</a>
    <a href="{{ route('presensi.susulan') }}" class="btn btn-danger">Presensi Susulan/Ubah Presensi</a>
</div>
</div>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('presensi.store') }}">
                @csrf
                <div class="row">
                @if(isset($jadwal))
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label>Hari/Tanggal</label>
                        <p>{{ $hari }}, {{ date('d/m/Y') }}</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label>Jadwal - Mata Pelajaran</label>
                        <select name="jadwal" id="jadwal" class="form-control select2" required>
                            <option value="" selected disabled>-- Pilih Jadwal --</option>
                            @foreach ($jadwal as $item)
                            <option value="{{ $item->id }}" data-kelas="{{ $item->data_kelas->nama_kelas }}">Jam {{ $item->jam_matpel }} -- {{ $item->data_kelas->nama_kelas }} ({{ $item->data_matpel->nama_matpel }} - {{ $item->data_matpel->bobot_matpel }} sks)</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @else
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label>Kelas</label>
                        <select name="kelas" id="kelas" class="form-control select2" required>
                            <option value="" selected disabled>-- Pilih Kelas --</option>
                            @foreach ($kelas as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label>Mata Pelajaran</label>
                        <select name="matpel" class="form-control select2" required>
                            <option value="" selected disabled>-- Pilih Matpel --</option>
                            @foreach ($matpel as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_matpel }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif
                </div>
                <hr>
                <div id="list-table">
                </div>
                {{-- @if(isset($request->kelas))
                <table class="table">
                    <tr>
                        <th>No.</th>
                        <th>Nama Siswa</th>
                        <th>Asrama</th>
                        <th>Kehadiran</th>
                    </tr>
                    @php
                    $i = 1;
                    @endphp
                    @foreach ($siswa as $item)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $item->nama_siswa }}</td>
                            <td>Asrama</td>
                            <td><input name="kehadiran[]" value="{{ $item->id }}"/></td>
                        </tr>
                    @endforeach
                </table>
                <button class="btn btn-success" type="submit" name="simpan">Simpan</button>
                @endif --}}
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
