<table class="table">
    <tr>
        <th>No.</th>
        <th>NIS</th>
        <th>Nama Siswa</th>
        <th>Nilai</th>
    </tr>
    @php
    $i = 1;   
    @endphp
    @foreach ($siswa as $item)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $item->data_siswa->nis }}</td>
            <td>{{ $item->data_siswa->nama_siswa }}</td>
            <td>
                <input type="hidden" name="id_siswa[]" value="{{ $item->id }}" readonly/>
                <input type="number" name="nilai_siswa[]" value="0" class="form-control" />
            </td>
        </tr>
    @endforeach
</table>
<button class="btn btn-success" type="submit" name="simpan">Simpan</button>