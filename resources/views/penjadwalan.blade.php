@extends('layouts.admin', ['halaman' => 'Penjadwalan Mata Pelajaran'])
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
  });
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('data-master.alert')
        <div class="card">
            <div class="card-body table-responsive">
                {{-- <a href="?acak" class="btn btn-success">Acak Jadwal</a> --}}
                <form action="{{ route('penjadwalan.index') }}" method="GET">
                    <button type="submit" name="acak" value="true" class="btn btn-success">Acak Jadwal</button>
                </form>
                <table class="table table-bordered">
                    <tr class="text-center bg-primary">
                        <th rowspan="2">Jam Matpel</th>
                        @foreach($kelas as $kls)
                        <th colspan="6">{{ $kls->nama_kelas }}</th>
                        @endforeach
                    </tr>
                    <tr class="text-center bg-secondary">
                        @foreach($kelas as $kls)
                        @foreach($harian as $hr)
                        <th>{{ $hr->hari }}</th>
                        @endforeach
                        @endforeach
                    </tr>
                    @for ($i = 0; $i < count($matriks); $i++)
                    <tr>
                        <td class="text-center">{{ $i+1 }}</td>
                        @for ($j = 0; $j < 24; $j++)
                        {{-- {{ dd(count($matriks[$i])) }} --}}
                        {{-- @if($j == 18 && $i == 8)
                        {{ dd($matriks[$i]) }}
                        @endif --}}
                        @if(!isset($matriks[$i][$j]))
                        <td>-</td>
                        @else
                        <td>{{ $matriks[$i][$j]->nama_guru }}</td>
                        @endif
                        @endfor
                    </tr>
                    @endfor
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

@endsection
