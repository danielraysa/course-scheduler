<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}{{ isset($halaman) ? ' - '.$halaman : '' }}</title>
    <link rel="icon" href="favicon.ico"/>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- pace-progress -->
    {{-- <link rel="stylesheet" href="{{ asset('plugins/pace-progress/themes/black/pace-theme-flat-top.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('plugins/pace-progress/themes/silver/pace-theme-flash.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <style>
        .text-sm .select2-container--default .select2-selection--single, select.form-control-sm~.select2-container--default .select2-selection--single {
            height: calc(2.2125rem + 2px);
        }
        .select2-container--default .select2-selection--single {
            padding: 0.56875rem 0.75rem;
        }
    </style>
    @stack('css')
</head>

<body data-scrollbar-auto-hide="n" class="hold-transition sidebar-mini layout-fixed pace-active text-sm">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-success navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link"><h5>{{ env('SCHOOL_NAME') }}</h5></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto pr-2">
                <!-- Notifications Dropdown Menu -->
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li> --}}
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-user mr-2"></i> {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item text-center">
                            {{-- <img src="{{ asset('embuh.png') }}" class="img-circle elevation-2 mb-3" alt="{{ Auth::user()->name }}"> --}}
                            <img src="https://ui-avatars.com/api/?name={{ Auth::user()->name }}" class="img-circle mx-auto elevation-2 mb-3" alt="{{ Auth::user()->name }}">
                            <h5 class="mb-0">{{ Auth::user()->name }}</h5>
                            @if(Auth::user()->data_guru)
                            <p class="mb-0 font-weight-bold">{{ Auth::user()->data_guru->nip }}</p>
                            @endif
                            @if(Auth::user()->data_siswa)
                            <p class="mb-0 font-weight-bold">{{ Auth::user()->data_siswa->nis }}</p>
                            @endif
                            <p class="mb-0">{{ Auth::user()->email }}</p>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt mr-2"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-success elevation-4">
            <!-- Brand Logo -->
            {{-- <a href="#" class="brand-link">
                <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                <span class="brand-text font-weight-bold">{{ config('app.name', 'Laravel') }}</span>
            </a> --}}
            <div class="w-50 text-center mx-auto mb-3">
                <img src="{{ asset('logo-smk-telkom.png') }}" class="w-75" />
            </div>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                {{-- <div class="user-panel mt-1 py-2 d-flex">
                    <div class="image">
                        <img src="https://adminlte.io/themes/v3/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                    </div>
                </div> --}}

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-collapse-hide-child" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="{{ route('home') }}" class="nav-link {{ (request()->is('home')) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-home"></i>
                                <p>Halaman Utama</p>
                            </a>
                        </li>
                        @if(Auth::user()->id == 1)
                        <li class="nav-item {{ (request()->is('admin/*')) ? 'menu-open' : '' }}">
                            {{-- <a href="#" class="nav-link {{ (request()->is('admin/*'))&& Route::currentRouteName() != 'penjadwalan.index' ? 'active' : '' }}"> --}}
                            <a href="#" class="nav-link {{ (request()->is('admin/*')) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>Data Master
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                {{-- <li class="nav-item">
                                    <a href="{{ route('konfigurasi.index') }}" class="nav-link {{ Route::currentRouteName() == 'konfigurasi.index' ? 'active' : '' }}">
                                        <i class="fas fa-cogs nav-icon"></i>
                                        <p>Konfigurasi Sistem</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('konstrain.index') }}" class="nav-link {{ Route::currentRouteName() == 'konstrain.index' ? 'active' : '' }}">
                                        <i class="fas fa-balance-scale nav-icon"></i>
                                        <p>Konstrain Penjadwalan</p>
                                    </a>
                                </li> --}}
                                <li class="nav-item">
                                    <a href="{{ route('semester.index') }}" class="nav-link {{ Route::currentRouteName() == 'semester.index' ? 'active' : '' }}">
                                        <i class="far fa-calendar-alt nav-icon"></i>
                                        <p>Semester</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('asrama.index') }}" class="nav-link {{ Route::currentRouteName() == 'asrama.index' ? 'active' : '' }}">
                                        <i class="fas fa-home nav-icon"></i>
                                        <p>Asrama</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('kelas.index') }}" class="nav-link {{ Route::currentRouteName() == 'kelas.index' ? 'active' : '' }}">
                                        <i class="fab fa-buffer nav-icon"></i>
                                        <p>Kelas</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('mata-pelajaran.index') }}" class="nav-link {{ Route::currentRouteName() == 'mata-pelajaran.index' ? 'active' : '' }}">
                                        <i class="fas fa-book nav-icon"></i>
                                        <p>Mata Pelajaran</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{ route('user.index') }}" class="nav-link {{ Route::currentRouteName() == 'user.index' ? 'active' : '' }}">
                                        <i class="fas fa-user-tie nav-icon"></i>
                                        <p>User</p>
                                    </a>
                                </li> --}}
                                <li class="nav-item">
                                    <a href="{{ route('guru.index') }}" class="nav-link {{ Route::currentRouteName() == 'guru.index' ? 'active' : '' }}">
                                        <i class="fas fa-user-tie nav-icon"></i>
                                        <p>Guru</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('wali-kelas.index') }}" class="nav-link {{ Route::currentRouteName() == 'wali-kelas.index' ? 'active' : '' }}">
                                        <i class="fas fa-chalkboard-teacher nav-icon"></i>
                                        <p>Wali Kelas</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('siswa.index') }}" class="nav-link {{ Route::currentRouteName() == 'siswa.index' ? 'active' : '' }}">
                                        <i class="fas fa-users nav-icon"></i>
                                        <p>Siswa</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('penempatan.index') }}" class="nav-link {{ Route::currentRouteName() == 'penempatan.index' ? 'active' : '' }}">
                                        <i class="fas fa-boxes nav-icon"></i>
                                        <p>Penempatan Kelas</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('penjadwalan.index') }}" class="nav-link {{ Route::currentRouteName() == 'penjadwalan.index' ? 'active' : '' }}">
                                        <i class="nav-icon far fa-calendar-alt"></i>
                                        <p>Penjadwalan</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        @if(Auth::user()->data_guru)
                        <li class="nav-item">
                            <a href="{{ route('presensi.index') }}" class="nav-link {{ Route::currentRouteName() == 'presensi.index' ? 'active' : '' }}">
                                <i class="nav-icon far fa-calendar-check"></i>
                                <p>Presensi</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('penilaian.index') }}" class="nav-link {{ Route::currentRouteName() == 'penilaian.index' ? 'active' : '' }}">
                                <i class="nav-icon fas fa-chart-bar"></i>
                                <p>Penilaian</p>
                                {{-- <p>Penilaian<span class="right badge badge-danger">New</span></p> --}}
                            </a>
                        </li>
                        @endif
                        {{-- @if(Auth::user()->id == 1 || Auth::user()->data_guru)
                        <li class="nav-item">
                            <a href="{{ route('report-nilai') }}" class="nav-link {{ Route::currentRouteName() == 'report-nilai' ? 'active' : '' }}">
                                <i class="nav-icon fas fa-book"></i>
                                <p>Rapor</p>
                            </a>
                        </li>
                        @endif --}}
                        @if((Auth::user()->data_guru && auth()->user()->data_guru->wali_kelas))
                        <li class="nav-item">
                            <a href="{{ route('rapor') }}" class="nav-link {{ Route::currentRouteName() == 'rapor' ? 'active' : '' }}">
                                <i class="nav-icon far fa-calendar-check"></i>
                                <p>Rapor</p>
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->is('laporan/*')) ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ (request()->is('laporan/*')) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-swatchbook"></i>
                                <p>Laporan
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('laporan.presensi') }}" class="nav-link {{ Route::currentRouteName() == 'laporan.presensi' ? 'active' : '' }}">
                                        <i class="nav-icon fas fa-calendar-day"></i>
                                        <p>Presensi</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('laporan.raport') }}" class="nav-link {{ Route::currentRouteName() == 'laporan.raport' ? 'active' : '' }}">
                                        <i class="nav-icon fas fa-graduation-cap"></i>
                                        <p>Penilaian</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        @if(Auth::user()->data_siswa)
                        <li class="nav-item">
                            <a href="{{ route('presensi.list') }}" class="nav-link {{ Route::currentRouteName() == 'presensi.list' ? 'active' : '' }}">
                                <i class="nav-icon far fa-calendar-check"></i>
                                <p>Presensi</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('rapor') }}" class="nav-link {{ Route::currentRouteName() == 'rapor' ? 'active' : '' }}">
                                <i class="nav-icon fas fa-book"></i>
                                <p>Rapor</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 id="title-page" class="m-0 text-dark">@if(isset($halaman)) {{ $halaman }} @else Starter Page @endif</h1>
                        </div><!-- /.col -->
                        @stack('menu-button')
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    @yield('content')

                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                {{-- {{ \Illuminate\Foundation\Inspiring::quote() }} --}}
                {{ env('SCHOOL_NAME') }}
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2020-{{ date('Y') }}</strong>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    {{-- <script src="plugins/jquery/jquery.min.js"></script> --}}
    <!-- Bootstrap 4 -->
    {{-- <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script> --}}
    <!-- AdminLTE App -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!-- pace-progress -->
    <script src="{{ asset('plugins/pace-progress/pace.min.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    <script>
    $('.select2').select2();
    /* $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    }); */
    </script>
</body>

</html>
