<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Siswa;
use Faker\Generator as Faker;

$factory->define(Siswa::class, function (Faker $faker) {
    return [
        'nis' => $faker->numberBetween(1000, 9999),
        'nama_siswa' => $faker->name
    ];
});
