<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Guru;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Guru::class, function (Faker $faker) {
    return [
        'nip' => $faker->numberBetween(1000, 9999),
        'nama_guru' => $faker->name
    ];
});
