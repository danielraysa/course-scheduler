<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenempatanKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penempatan_kelas', function (Blueprint $table) {
            $table->id();
            $table->integer('semester_id')->nullable();
            $table->integer('kelas_id')->nullable();
            $table->integer('asrama_id')->nullable();
            $table->integer('siswa_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penempatan_kelas');
    }
}
