<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presensi_siswa', function (Blueprint $table) {
            $table->id();
            $table->integer('jadwal_id')->nullable();
            $table->integer('guru_id')->nullable();
            $table->integer('matpel_id')->nullable();
            $table->integer('kelas_id')->nullable();
            $table->integer('siswa_id');
            $table->char('status', 1);
            $table->datetime('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presensi_siswa');
    }
}
