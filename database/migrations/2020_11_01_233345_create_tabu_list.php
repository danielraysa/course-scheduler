<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabuList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabu_list', function (Blueprint $table) {
            $table->id();
            $table->integer('semester_id');
            $table->integer('matpel_id');
            $table->integer('guru_id');
            $table->integer('kelas_id');
            $table->integer('hari');
            $table->integer('jam_pelajaran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabu_list');
    }
}
