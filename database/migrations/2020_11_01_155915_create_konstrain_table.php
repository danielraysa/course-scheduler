<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKonstrainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konstrain', function (Blueprint $table) {
            $table->id();
            $table->string('hari');
            $table->integer('max_jam_matpel');
            $table->integer('max_jam_berurutan');
            $table->integer('max_guru_mengajar');
            $table->integer('max_bobot_matpel');
            $table->integer('max_iterasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konstrain');
    }
}
