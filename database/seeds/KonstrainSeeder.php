<?php

use Illuminate\Database\Seeder;

class KonstrainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('konstrain')->insert([
            'hari' => 'Senin',
            'max_jam_matpel' => 9,
            'max_jam_berurutan' => 3,
            'max_guru_mengajar' => 9,
            'max_bobot_matpel' => 25,
            'max_iterasi' => 50,
        ]);
        DB::table('konstrain')->insert([
            'hari' => 'Selasa',
            'max_jam_matpel' => 9,
            'max_jam_berurutan' => 3,
            'max_guru_mengajar' => 9,
            'max_bobot_matpel' => 25,
            'max_iterasi' => 50,
        ]);
        DB::table('konstrain')->insert([
            'hari' => 'Rabu',
            'max_jam_matpel' => 9,
            'max_jam_berurutan' => 3,
            'max_guru_mengajar' => 9,
            'max_bobot_matpel' => 25,
            'max_iterasi' => 50,
        ]);
        DB::table('konstrain')->insert([
            'hari' => 'Kamis',
            'max_jam_matpel' => 9,
            'max_jam_berurutan' => 3,
            'max_guru_mengajar' => 9,
            'max_bobot_matpel' => 25,
            'max_iterasi' => 50,
        ]);
        DB::table('konstrain')->insert([
            'hari' => 'Sabtu',
            'max_jam_matpel' => 6,
            'max_jam_berurutan' => 3,
            'max_guru_mengajar' => 9,
            'max_bobot_matpel' => 25,
            'max_iterasi' => 50,
        ]);
        DB::table('konstrain')->insert([
            'hari' => 'Minggu',
            'max_jam_matpel' => 3,
            'max_jam_berurutan' => 3,
            'max_guru_mengajar' => 9,
            'max_bobot_matpel' => 25,
            'max_iterasi' => 50,
        ]);
    }
}
