<?php

use App\Models\JenisKelas;
use App\Models\Kelas;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $jenis_kelas = [
            [
                'long' => 'Rekayasa Perangkat Lunak',
                'short' => 'RPL'
            ],
            [
                'long' => 'Teknik Komputer dan Jaringan',
                'short' => 'TKJ'
            ],
            [
                'long' => 'Multimedia',
                'short' => 'MM'
            ]
        ];
        foreach ($jenis_kelas as $value) {
            $angkatan = [10, 11, 12];
            $jenis_kelas = JenisKelas::create([
                'nama_jenis' => $value['long'],
            ]);
            foreach ($angkatan as $item) {
                $pararel_kelas = [1, 2];
                foreach ($pararel_kelas as $kls) {
                    Kelas::create([
                        'jenis_id' => $jenis_kelas->id,
                        'nama_kelas' => $item.'-RPL-'.$kls,
                        'kuota_kelas' => 40,
                        'status_kelas' => Kelas::AKTIF,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }

        /* Kelas::create([
            'jenis_id' => 1,
            'nama_kelas' => '10-RPL-1',
            'kuota_kelas' => 40,
            'status_kelas' => Kelas::AKTIF,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        Kelas::create([
            'jenis_id' => 1,
            'nama_kelas' => '10-RPL-2',
            'kuota_kelas' => 38,
            'status_kelas' => Kelas::AKTIF,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        Kelas::create([
            'jenis_id' => 2,
            'nama_kelas' => '10-TKJ-1',
            'kuota_kelas' => 40,
            'status_kelas' => Kelas::AKTIF,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        Kelas::create([
            'jenis_id' => 2,
            'nama_kelas' => '10-TKJ-2',
            'kuota_kelas' => 40,
            'status_kelas' => Kelas::AKTIF,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        Kelas::create([
            'jenis_id' => 2,
            'nama_kelas' => '10-TKJ-3',
            'kuota_kelas' => 40,
            'status_kelas' => Kelas::AKTIF,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        Kelas::create([
            'jenis_id' => 3,
            'nama_kelas' => '10-MM-1',
            'kuota_kelas' => 40,
            'status_kelas' => Kelas::AKTIF,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        Kelas::create([
            'jenis_id' => 3,
            'nama_kelas' => '10-MM-2',
            'kuota_kelas' => 40,
            'status_kelas' => Kelas::AKTIF,
            'created_at' => date('Y-m-d H:i:s')
        ]); */
    }
}
