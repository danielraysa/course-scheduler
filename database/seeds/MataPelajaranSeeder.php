<?php

use Illuminate\Database\Seeder;
use App\Models\MataPelajaran;

class MataPelajaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /* $nama = [
            'Pemrograman',
            'Jaringan',
            'IPA',
            'IPS',
            'Multimedia',
            'Akuntansi',
            'Pariwisata',
            'Sejarah',
            'Geografi',
            'Bahasa Indonesia',
            'Bahasa Inggris',
        ];
        foreach($nama as $matpel){
            MataPelajaran::create([
                'nama_matpel' => $matpel,
                'status_matpel' => MataPelajaran::AKTIF,
                'bobot_matpel' => 3,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        } */
        MataPelajaran::create([
            'nama_matpel' => 'Simulsi Dan Komunikasi Digital',
            'bobot_matpel' => 3,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Sistem Komputer',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pengembangan Diri',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Produk Kreatif & Kewirausahawan',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => "Qur'an Hadist",
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Teknik Audio Video',
            'bobot_matpel' => 12,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Fiqih',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Aqidah Akhlak',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pendidikan Agama & Budi Pekerti',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Administrasi Infrastruktur Jaringan',
            'bobot_matpel' => 6,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Teknologi Jaringan Berbasis Luas (WAN)',
            'bobot_matpel' => 6,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Teknologi Layanan Jaringan',
            'bobot_matpel' => 5,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'SKI - Kedarululuman',
            'bobot_matpel' => 1,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Desain Media Interaktif',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pemograman Web & Perangkat Bergerak',
            'bobot_matpel' => 7,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Basis Data',
            'bobot_matpel' => 4,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Desain Grafis Percetakan',
            'bobot_matpel' => 11,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Desain Grafis',
            'bobot_matpel' => 3,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Seni Budaya',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Bahasa Inggris',
            'bobot_matpel' => 3,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pemograman Dasar',
            'bobot_matpel' => 3,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pendidikan Jasmani & Kesehatan',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Sejarah Indonesia',
            'bobot_matpel' => 3,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pendidikan Kewarganegaraan',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Kimia',
            'bobot_matpel' => 3,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Ilmu Tajwid',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pendidikan Agama & Budi Pekerti',
            'bobot_matpel' => 1,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Nahwu Shorof',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Sistem Telekomunikasi',
            'bobot_matpel' => 1,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pemograman Berorientasi Objek',
            'bobot_matpel' => 8,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Fisika',
            'bobot_matpel' => 3,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Teknik Animasi 23D',
            'bobot_matpel' => 12,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Komputer & Jaringan Dasar',
            'bobot_matpel' => 5,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Matematika',
            'bobot_matpel' => 4,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Bahasa Daerah',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Desain Media Interaktif',
            'bobot_matpel' => 13,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pemograman Web & Perangkat Bergerak',
            'bobot_matpel' => 13,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'English Morning',
            'bobot_matpel' => 1,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'BAR',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Bahasa Indonesia',
            'bobot_matpel' => 4,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Bahasa Arab',
            'bobot_matpel' => 1,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Bahasa Arab',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Aqidah Akhlak',
            'bobot_matpel' => 1,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Produk Kreatif & Kewirausahaan',
            'bobot_matpel' => 4,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Bahasa Indonesia',
            'bobot_matpel' => 3,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Pemodelan Perangkat Lunak',
            'bobot_matpel' => 4,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Administrasi Sistem Jaringan',
            'bobot_matpel' => 6,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Administrasi Infrastruktur Jaringan',
            'bobot_matpel' => 9,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Teknologi Layanan Jaringan',
            'bobot_matpel' => 8,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Produk Kreatif & Kewirausahaan',
            'bobot_matpel' => 6,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Bahasa Inggris',
            'bobot_matpel' => 4,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Bahasa Indonesia',
            'bobot_matpel' => 2,
            'status_matpel' => 'Aktif'
            ]);
    MataPelajaran::create([
            'nama_matpel' => 'Administrasi Sistem Jaringan',
            'bobot_matpel' => 8,
            'status_matpel' => 'Aktif'
            ]);
    }
}
