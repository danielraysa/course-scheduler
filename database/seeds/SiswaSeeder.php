<?php

use Illuminate\Database\Seeder;
use App\Models\PenempatanKelas;
use App\Models\User;
use App\Models\Siswa;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $siswa = factory(\App\Models\Siswa::class, 210)->create();
        /* $x = 1;
        for($i = 1; $i <= 210; $i++){
            $faker = Faker::create('id_ID');

            $user = User::create([
                'name' => $faker->name,
                'email' => 'siswa'.$i.'@mail.com',
                'password' => bcrypt('password')
            ]);
            $siswa = Siswa::create([
                'user_id' => $user->id,
                'nis' => $faker->numberBetween(1000, 9999),
                'nama_siswa' => $faker->name,
                'status_siswa' => Siswa::AKTIF
            ]);
            PenempatanKelas::create([
                'semester_id' => 1,
                'kelas_id' => $i % 30 == 0 ? $x++ : $x,
                'asrama_id' => $i % 50 == 0 ? $x++ : $x,
                'siswa_id' => $siswa->id,
                // 'siswa_id' => $i,
            ]);
        } */
        $db_sisa = DB::select("SELECT * from kelas k where id not in (select kelas_id from penempatan_kelas pk )");

        $x = 210;
        foreach ($db_sisa as $key => $value) {
            for($i = 1; $i <= 30; $i++){
                $faker = Faker::create('id_ID');

                $user = User::create([
                    'name' => $faker->name,
                    'email' => 'siswa'.($i+$x).'@mail.com',
                    'password' => bcrypt('password')
                ]);
                $siswa = Siswa::create([
                    'user_id' => $user->id,
                    'nis' => $faker->numberBetween(1000, 9999),
                    'nama_siswa' => $faker->name,
                    'status_siswa' => Siswa::AKTIF
                ]);
                PenempatanKelas::create([
                    'semester_id' => 1,
                    'kelas_id' => $value->id,
                    'asrama_id' => 5,
                    'siswa_id' => $siswa->id,
                    // 'siswa_id' => $i,
                ]);
            }
            $x+=30;
        }
    }
}
