<?php

use App\Models\Semester;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* DB::table('semester')->insert([
            'periode' => '2020/2021',
            'semester' => '1',
            'status' => Semester::AKTIF,
        ]);
        $this->call(UserSeeder::class);
        $this->call(KonstrainSeeder::class);
        $this->call(KonfigurasiSeeder::class);
        $this->call(MataPelajaranSeeder::class);
        $this->call(AsramaSeeder::class);
        $this->call(KelasSeeder::class);
        $this->call(GuruSeeder::class); */
        $this->call(SiswaSeeder::class);
        // $this->call(UserSiswaSeeder::class);
    }
}
