<?php

use Illuminate\Database\Seeder;
use App\Models\Asrama;

class AsramaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $nama = [
            'Asrama A',
            'Asrama B',
            'Asrama C',
            'Asrama D',
            'Asrama Muzam',
        ];
        foreach($nama as $asrama){
            Asrama::create([
                'nama_asrama' => $asrama,
                'status_asrama' => Asrama::AKTIF
            ]);
        }
    }
}
