<?php

use Illuminate\Database\Seeder;

class KonfigurasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('konfigurasi')->insert([
            'parameter' => 'nama_sekolah',
            'value' => 'SMK 1 Jombang',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('konfigurasi')->insert([
            'parameter' => 'alamat',
            'value' => 'Jl. Jombang',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        /* DB::table('konfigurasi')->insert([
            'parameter' => 'jam_mulai',
            'value' => 45,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('konfigurasi')->insert([
            'parameter' => 'satuan_jam',
            'value' => 45,
            'created_at' => date('Y-m-d H:i:s')
        ]); */
        DB::table('konfigurasi')->insert([
            'parameter' => 'path_gambar',
            'value' => 'public/image.png',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('konfigurasi')->insert([
            'parameter' => 'path_logo',
            'value' => 'public/logo.png',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('persentase_nilai')->insert([
            'semester_id' => 1,
            'tugas' => 20,
            'harian' => 20,
            'uts' => 30,
            'uas' => 30,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
