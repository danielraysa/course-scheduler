<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Guru;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $guru = factory(\App\Models\Guru::class, 30)->create();
        /* for($i = 0; $i < 30; $i++){
            $faker = Faker::create('id_ID');

            $user = User::create([
                'name' => $faker->name,
                'email' => 'guru'.$i.'@mail.com',
                'password' => bcrypt('password')
            ]);
            $guru = Guru::create([
                'user_id' => $user->id,
                'nip' => $faker->numberBetween(1000, 9999),
                'nama_guru' => $faker->name,
                'status_guru' => Guru::AKTIF
            ]);
        } */
        $raw_query = <<<SQL
        INSERT INTO guru (nip,nama_guru,status_guru) VALUES
	 ('5330','Hj.INDIRA ZULAIKHA,S.Pd','Aktif'),
	 ('1605','Izzudin, LC','Aktif'),
	 ('2426','Drs. MUDHOFAR, M.Pd','Aktif'),
	 ('2826','H. KUSAERI,S.PdI','Aktif'),
	 ('3973','MU''ADIN. SAg.','Aktif'),
	 ('8681','PARTOYO, S.Kom','Aktif'),
	 ('2546','MUSTAFID KHILMI, MT','Aktif'),
	 ('5181','Drs.H. MASYKUR Ks,M.Si','Aktif'),
	 ('9059','YULIA CH., Skom.','Aktif'),
	 ('4728','HARYANTO ARBI, S.Si.','Aktif'),
	 ('5771','ROSMIYATUL FAKHIROH, S.Pd','Aktif'),
	 ('2632','ELYA DWI L, S.Pd.','Aktif'),
	 ('5771','RAHMAD PRIBADI, S.Pd','Aktif'),
	 ('5055','SITI MASRIKHAH, S.Pd.','Aktif'),
	 ('6762','TITIK ISBANDIYAH, SPd.','Aktif'),
	 ('3444','Dra. NUR LAILA','Aktif'),
	 ('3573','SLAMET RIYANTO,S.Kom','Aktif'),
	 ('1941','Drs. SENTOT AGUS S.','Aktif'),
	 ('6416','Drs. BADRUS S.','Aktif'),
	 ('9067','Dra. DWI WAHYU U.','Aktif'),
	 ('3170','FATKHUR ROZI, SPd.','Aktif'),
	 ('3064','Dra. NURWAHYUNINGSIH','Aktif'),
	 ('6639','IMAM SIBAWEH, SH,MH','Aktif'),
	 ('5641','NUNUK NURHAYATI, ST','Aktif'),
	 ('3121','MOH. UMAR, S.Ag.','Aktif'),
	 ('5585','AHMAD ASY''ARI,SIP','Aktif'),
	 ('3855','ASRORI ROFIQ,S.Pd.I','Aktif'),
	 ('1675','AHMAD KHOIRON, S.Ag','Aktif'),
	 ('8879','AINUL YAQIN, ST.','Aktif'),
	 ('1231','Ir. NURKOYIN, M.KOM','Aktif'),
	 ('1232','Hj.INDIRA ZULAIKHA,S.Pd','Aktif'),
	 ('1233','Izzudin, LC','Aktif'),
	 ('1234','Drs. MUDHOFAR, M.Pd','Aktif'),
	 ('1235','H. KUSAERI,S.PdI','Aktif'),
	 ('1236','MU''ADIN. SAg.','Aktif'),
	 ('1237','PARTOYO, S.Kom','Aktif'),
	 ('1238','MUSTAFID KHILMI, MT','Aktif'),
	 ('1239','Drs.H. MASYKUR Ks,M.Si','Aktif'),
	 ('1241','YULIA CH., Skom.','Aktif'),
	 ('1242','HARYANTO ARBI, S.Si.','Aktif'),
	 ('1243','ROSMIYATUL FAKHIROH, S.Pd','Aktif'),
	 ('1244','ELYA DWI L, S.Pd.','Aktif'),
	 ('1245','RAHMAD PRIBADI, S.Pd','Aktif'),
	 ('1246','SITI MASRIKHAH, S.Pd.','Aktif'),
	 ('1247','TITIK ISBANDIYAH, SPd.','Aktif'),
	 ('1248','Dra. NUR LAILA','Aktif'),
	 ('1249','SLAMET RIYANTO,S.Kom','Aktif'),
	 ('1251','Drs. SENTOT AGUS S.','Aktif'),
	 ('1252','Drs. BADRUS S.','Aktif'),
	 ('1253','Dra. DWI WAHYU U.','Aktif'),
	 ('1254','FATKHUR ROZI, SPd.','Aktif'),
	 ('1255','Dra. NURWAHYUNINGSIH','Aktif'),
	 ('1256','IMAM SIBAWEH, SH,MH','Aktif'),
	 ('1257','NUNUK NURHAYATI, ST','Aktif'),
	 ('1258','MOH. UMAR, S.Ag.','Aktif'),
	 ('1259','AHMAD ASY''ARI,SIP','Aktif'),
	 ('1261','ASRORI ROFIQ,S.Pd.I','Aktif'),
	 ('1262','AHMAD KHOIRON, S.Ag','Aktif'),
	 ('1263','AINUL YAQIN, ST.','Aktif'),
	 ('1264','ARIF BAKHTIYAR, S.Pd.','Aktif'),
	 ('1265','NANING EVA, S.Kom','Aktif'),
	 ('1266','NURHAYATI, SE','Aktif'),
	 ('1267','NURUL LAILA, S.Pd','Aktif'),
	 ('1268','LUTFIYANAH V., S.Pd','Aktif'),
	 ('1269','ELIS ANDRIANA,S.Kom','Aktif'),
	 ('1271','IMAM SYAFI''I, S.Kom','Aktif'),
	 ('1272','SYAMSUL HUDA,S.Pd','Aktif'),
	 ('1273','SRI UTAMI,S.Pd','Aktif'),
	 ('1274','DINIKE AGUSTIN,S.Pd','Aktif'),
	 ('1275','MUFDLIATUL ISTIANAH, S.Pd.I','Aktif'),
	 ('1276','NASRULLOH FAHMI, S.Pd','Aktif'),
	 ('1277','Anas Turmudzi, S.Kom','Aktif'),
	 ('1278','Muslichul Arwani, S.Kom','Aktif'),
	 ('1279','Elly Novito Sari, S.Pd','Aktif'),
	 ('1281','Rahmatillah Nur Mubariza, S.Ds','Aktif'),
	 ('1282','M. Muslih Farid S.Sos','Aktif'),
	 ('1283','VENNY AIMMATIN N.J, M.Pd','Aktif'),
	 ('1284','H.ZAENAL ARIFIN, S.Ag.','Aktif'),
	 ('1285','NUR AFIFAH, S.Pd','Aktif'),
	 ('1286','ANDRIANTO SETIAWAN, S.Kom','Aktif'),
	 ('1287','Kristina Dwi Lestari, S.Pd','Aktif'),
	 ('1288','Ayu Retno','Aktif'),
	 ('1289','Khariri, S.Kom','Aktif'),
	 ('1291','Agus Ainul Yaqin','Aktif'),
	 ('1292','Ayu Adelina Suyono, S.Kom','Aktif'),
	 ('1293','Muhammad Lutfi, S.Pd','Aktif'),
	 ('1294','Angga Setiya Budi','Aktif'),
	 ('1295','Citra Kusuma Wijaya','Aktif'),
	 ('1296','Drs. BADRUS S.','Aktif');
SQL;
        DB::query($raw_query);
        $guru = Guru::all();
        $i = 1;
        foreach ($guru as $key => $value) {
            $user = User::create([
                'name' => $value->nama_guru,
                'email' => 'guru'.$i.'@mail.com',
                'password' => bcrypt('password')
            ]);
            $value->update([
                'user_id' => $user->id,
            ]);
            $i++;
        }
    }
}
