<?php

use App\Models\Asrama;
use App\Models\Kelas;
use Illuminate\Database\Seeder;
use App\Models\PenempatanKelas;
use App\Models\User;
use App\Models\Siswa;
use Faker\Factory as Faker;
use Illuminate\Support\Str;

class PenempatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $siswa = factory(\App\Models\Siswa::class, 210)->create();
        $kelas = Kelas::all();
        $asrama = Asrama::all();
        // $x = 1;
        // for($i = 1; $i <= 210; $i++){
        $i = 1;
        foreach ($kelas as $item) {
            $faker = Faker::create('id_ID');
            $user = User::create([
                'name' => $faker->name,
                'email' => 'siswa'.$i.'@mail.com',
                'password' => bcrypt('password')
            ]);
            $siswa = Siswa::create([
                'user_id' => $user->id,
                'nis' => $faker->numberBetween(1000, 9999),
                'nama_siswa' => $faker->name,
                'status_siswa' => Siswa::AKTIF
            ]);
            PenempatanKelas::create([
                'semester_id' => 1,
                'kelas_id' => $item->id,
                'asrama_id' => $asrama[0]->id,
                'siswa_id' => $siswa->id,
                // 'siswa_id' => $i,
            ]);
            /* PenempatanKelas::create([
                'semester_id' => 1,
                'kelas_id' => $i % 30 == 0 ? $x++ : $x,
                'asrama_id' => $i % 50 == 0 ? $x++ : $x,
                'siswa_id' => $siswa->id,
                // 'siswa_id' => $i,
            ]); */
            $i++;
        }
    }
}
