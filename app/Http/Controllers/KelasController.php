<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\JenisKelas;
use App\Models\Semester;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jenis_kelas = JenisKelas::all();
        $kelas = Kelas::all();
        $semester = Semester::first();
        $list_kuota = DB::table('penempatan_kelas')->select('kelas_id',  DB::raw('count(*) as total'))->where('semester_id', $semester->id)->groupBy('kelas_id')->get();
        return view('data-master.kelas', compact('jenis_kelas','kelas','list_kuota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        if(isset($request->jenis_kelas)){
            $cek_jenis = JenisKelas::where('nama_jenis', $request->nama_jenis)->first();
            if ($cek_jenis) {
                return redirect()->route('kelas.index')->with('error', 'Data jurusan sudah ada');
            }
            $simpan = JenisKelas::create([
                'nama_jenis' => $request->nama_jenis,
            ]);
        }
        if(isset($request->kelas)){
            $cek_kelas = Kelas::where('nama_kelas', $request->nama_kelas)->first();
            if ($cek_kelas) {
                return redirect()->route('kelas.index')->with('error', 'Data kelas sudah ada');
            }
            $simpan = Kelas::create([
                'jenis_id' => $request->jenis,
                'nama_kelas' => $request->nama_kelas,
                'kuota_kelas' => $request->kuota_kelas,
                'status_kelas' => Kelas::AKTIF,
            ]);
        }
        return redirect()->route('kelas.index')->with('status', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function show($kelas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function edit($kelas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kelas = Kelas::find($id);
        if(isset($request->jenis_kelas)){
            $cek_jenis = JenisKelas::where('nama_jenis', $request->nama_jenis)->first();
            if ($cek_jenis) {
                return redirect()->route('kelas.index')->with('error', 'Data jurusan sudah ada');
            }
            JenisKelas::find($kelas)->update([
                'nama_jenis' => $request->nama_jenis,
            ]);
        }
        if(isset($request->kelas)){
            $cek_kelas = Kelas::where('nama_kelas', $request->nama_kelas)->first();
            if ($cek_kelas && $kelas->nama_kelas != $request->nama_kelas) {
                return redirect()->route('kelas.index')->with('error', 'Data kelas sudah ada');
            }
            $kelas->update([
                'jenis_id' => $request->jenis,
                'nama_kelas' => $request->nama_kelas,
                'kuota_kelas' => $request->kuota_kelas,
            ]);
        }
        return redirect()->route('kelas.index')->with('status', 'Berhasil memperbarui data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function destroy($kelas)
    {
        //
        Kelas::find($kelas)->update([
            'status_kelas' => Kelas::TIDAK_AKTIF,
        ]);
        return redirect()->route('kelas.index')->with('status', 'Berhasil menonaktifkan data kelas');
    }

    public function aktifkan($id)
    {
        //
        Kelas::find($id)->update([
            'status_kelas' => Kelas::AKTIF,
        ]);
        return redirect()->route('kelas.index')->with('status', 'Berhasil mengaktifkan data');
    }
}
