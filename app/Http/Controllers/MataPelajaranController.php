<?php

namespace App\Http\Controllers;

use App\Models\MataPelajaran;
use Illuminate\Http\Request;

class MataPelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $matpel = MataPelajaran::all();
        return view('data-master.matpel', compact('matpel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_matpel' => 'required|unique:mata_pelajaran',
        ]);
        $simpan = MataPelajaran::create([
            'nama_matpel' => $request->nama_matpel,
            'nilai_kkm' => $request->nilai_kkm,
            'bobot_matpel' => $request->bobot,
            'status_matpel' => 'Aktif',
        ]);
        return redirect()->route('mata-pelajaran.index')->with('status', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MataPelajaran  $mataPelajaran
     * @return \Illuminate\Http\Response
     */
    public function show(MataPelajaran $mataPelajaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MataPelajaran  $mataPelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit(MataPelajaran $mataPelajaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MataPelajaran  $mataPelajaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MataPelajaran $mataPelajaran)
    {
        //
        // dd($request->all(), $mataPelajaran);
        $mataPelajaran->update([
            'nama_matpel' => $request->nama_matpel,
            'nilai_kkm' => $request->nilai_kkm,
            'bobot_matpel' => $request->bobot,
        ]);
        return redirect()->route('mata-pelajaran.index')->with('status', 'Berhasil mengubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MataPelajaran  $mataPelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(MataPelajaran $mataPelajaran)
    {
        //
        $mataPelajaran->update([
            'status_matpel' => 'Tidak Aktif',
        ]);
        return redirect()->route('mata-pelajaran.index')->with('status', 'Berhasil menghapus data');
    }

    public function aktifkan($id)
    {
        MataPelajaran::find($id)->update([
            'status_matpel' => 'Aktif'
        ]);
        return redirect()->route('mata-pelajaran.index')->with('status', 'Berhasil mengubah status mata pelajaran');

    }
}
