<?php

namespace App\Http\Controllers;

use App\Helper\KalkulasiNilai;
use App\Models\JadwalPelajaran;
use Illuminate\Http\Request;
use App\Models\NilaiSiswa;
use App\Models\Presensi;
use App\Models\Siswa;
use App\Models\Semester;
use App\Models\MataPelajaran;
use App\Models\PenempatanKelas;
use App\Models\PersentaseNilai;
use App\Models\Kelas;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade\Pdf;

class RaporController extends Controller
{
    //
    public function index(Request $request)
    {
        $user = Auth::user();
        if($user->data_siswa){
            $data['semester'] = Semester::all();
            // $semester = Semester::first();
            $data['persentase'] = PersentaseNilai::first();
            $data_siswa = $user->data_siswa;
            // $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('siswa_id', $data_siswa->id)->first();
            if($request->semester){
                $matpel = $this->matpel_semester($data_siswa->id, $request->semester);
                $data['request'] = $request;
            }else{
                $matpel = $this->matpel_semester($data_siswa->id);
            }
            if($request->cetak){
                return $this->cetak($data_siswa->id, $request);
            }
            $data['matpel'] = $matpel;
            $presensi = $this->presensi_semester($data_siswa->id);
            $data['presensi_total'] = $presensi->count();
            $data['presensi_hadir'] = $presensi->where('status', 'H')->count();
            $data['presensi_alpha'] = $presensi->where('status', 'A')->count();

            return view('siswa.rapor', $data);
        }
        if($user->data_guru->wali_kelas){
            $semester = Semester::first();
            $kelas = $user->data_guru->wali_kelas->data_kelas;
            $penempatan = PenempatanKelas::with('data_siswa')->where('semester_id', $semester->id)->where('kelas_id', $kelas->id)->get();
            return view('guru.raport', compact('penempatan', 'kelas'));
        }
        return redirect('home');
    }

    public function show($siswa_id, Request $request)
    {
        $data['semester'] = Semester::all();
        // $semester = Semester::first();
        $data['persentase'] = PersentaseNilai::first();
        $data_siswa = Siswa::find($siswa_id);
        // $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('siswa_id', $data_siswa->id)->first();
        if($request->semester){
            $matpel = $this->matpel_semester($data_siswa->id, $request->semester);
            $data['request'] = $request;
        }else{
            $matpel = $this->matpel_semester($data_siswa->id);
        }
        $data['matpel'] = $matpel;
        $presensi = $this->presensi_semester($data_siswa->id);
        $data['presensi_total'] = $presensi->count();
        $data['presensi_hadir'] = $presensi->where('status', 'H')->count();
        $data['presensi_alpha'] = $presensi->where('status', 'A')->count();
        $data['presensi_ijin'] = $presensi->where('status', 'I')->count();
        $data['presensi_sakit'] = $presensi->where('status', 'S')->count();
        $data['id'] = $siswa_id;
        $data['data_siswa'] = $data_siswa;
        return view('guru.raport-detail', $data);
    }

    public function matpel_semester($siswa_id, $semester_id = null)
    {
        if($semester_id == null){
            $semester_id = Semester::first()->id;
        }
        $penempatan = PenempatanKelas::where('semester_id', $semester_id)->where('siswa_id', $siswa_id)->get()->pluck('kelas_id');
        $jadwal_matpel = JadwalPelajaran::where('semester_id', $semester_id)->where('kelas_id', $penempatan)->get()->pluck('matpel_id');
        $matpel = MataPelajaran::with(['data_nilai' => function($query) use ($semester_id, $siswa_id) {
            $query->where('semester_id', $semester_id)->where('siswa_id', $siswa_id);
        }])->whereIn('id', $jadwal_matpel)->get();
        return $matpel;
    }

    public function presensi_semester($siswa_id, $semester_id = null)
    {
        if($semester_id == null){
            $semester_id = Semester::first()->id;
        }
        $presensi = Presensi::where('siswa_id', $siswa_id)->get();

        return $presensi;
    }

    public function report_nilai(Request $request)
    {
        // $semester = Semester::first();
        $semester = Semester::whereAktif()->first();
        $matpel = MataPelajaran::first();
        $data['matpel'] = MataPelajaran::all();
        $data['kelas'] = Kelas::all();
        $data['persentase'] = PersentaseNilai::first();
        if($request->kelas && $request->matpel){
            $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get()->pluck('siswa_id');
            // dd($penempatan);
            $siswa = Siswa::with(['data_nilai' => function($query) use ($semester, $request) {
                // $query->where('semester_id', $semester->id)->where('matpel_id', $matpel->id)->where('kategori', 'tugas');
                $query->where('semester_id', $semester->id)->where('matpel_id', $request->matpel);
            }])->whereIn('id', $penempatan)->get();
            $data['siswa'] = $siswa;
            $data['request'] = $request;
        }
        // dd($siswa->data_nilai->nilai_tugas());
        return view('laporan.raport', $data);
    }

    function cetak($id, Request $request)
    {
        $siswa = Siswa::find($id);
        $semester = Semester::with('persentase_nilai')->first();
        if($request->semester){
            $semester = Semester::with('persentase_nilai')->find($request->semester);
        }
        $penempatan = PenempatanKelas::with('data_kelas.jns_kelas')->where('semester_id', $semester->id)->where('siswa_id', $siswa->id)->first();
        $matpel = collect($this->matpel_semester($siswa->id, $semester->id));
        $matpel_keagamaan = $matpel->filter(function ($item) {
            return $item->status_matpel == MataPelajaran::AKTIF && $item->jenis == 'keagamaan';
        })->map(function ($item) use ($semester) {
            $nilai = KalkulasiNilai::averageNilai($item, $semester->persentase_nilai);
            $item->nilai = $nilai;
            return $item;
        });
        $matpel_umum = $matpel->filter(function ($item) {
            return $item->status_matpel == MataPelajaran::AKTIF && $item->jenis != 'keagamaan';
        })->map(function ($item) use ($semester) {
            $nilai = KalkulasiNilai::averageNilai($item, $semester->persentase_nilai);
            $item->nilai = $nilai;
            return $item;
        });
        $data['semester'] = $semester;
        $data['siswa'] = $siswa;
        $data['matpel_umum'] = $matpel_umum;
        $data['matpel_keagamaan'] = $matpel_keagamaan;
        $data['kelas'] = $penempatan->data_kelas;


        $presensi = $this->presensi_semester($id);
        $data['presensi_total'] = $presensi->count();
        $data['presensi_hadir'] = $presensi->where('status', 'H')->count();
        $data['presensi_alpha'] = $presensi->where('status', 'A')->count();
        $data['presensi_ijin'] = $presensi->where('status', 'I')->count();
        $data['presensi_sakit'] = $presensi->where('status', 'S')->count();

        $pdf = Pdf::loadView('pdf.raport', $data);
        return $pdf->stream();
    }
}
