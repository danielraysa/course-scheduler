<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guru;
use App\Models\WaliKelas;
use App\Models\Semester;
use App\Models\Kelas;

class WaliKelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $semester = Semester::all();
        $guru = Guru::where('status_guru', Guru::AKTIF)->get();
        $walikelas = WaliKelas::all();
        $kelas = Kelas::all();
        return view('data-master.wali-kelas', compact('guru','walikelas','kelas','semester'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = WaliKelas::where('semester_id', $request->semester)->where('kelas_id', $request->kelas)->first();
        if($check){
            $kelas = Kelas::find($request->kelas);
            return redirect()->route('wali-kelas.index')->with('error', 'Data wali kelas '.$kelas->nama_kelas.' sudah terisi');
        }
        $check_guru = WaliKelas::where('semester_id', $request->semester)->where('guru_id', $request->guru)->first();
        if($check_guru){
            $guru = Guru::find($request->guru);
            return redirect()->route('wali-kelas.index')->with('error', 'Guru '.$guru->nama_guru.' sudah terdaftar');
        }

        WaliKelas::create([
            'semester_id' => $request->semester,
            // 'semester_id' => $semester->id,
            'guru_id' => $request->guru,
            'kelas_id' => $request->kelas,
        ]);
        return redirect()->route('wali-kelas.index')->with('status', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WaliKelas  $walikelas
     * @return \Illuminate\Http\Response
     */
    public function show(WaliKelas $guru)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WaliKelas  $walikelas
     * @return \Illuminate\Http\Response
     */
    public function edit(WaliKelas $walikelas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WaliKelas  $walikelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = WaliKelas::where('semester_id', $request->semester)->where('kelas_id', $request->kelas)->first();
        if($check){
            $kelas = Kelas::find($request->kelas);
            return redirect()->route('wali-kelas.index')->with('error', 'Data wali kelas '.$kelas->nama_kelas.' sudah terisi');
        }
        $check_guru = WaliKelas::where('semester_id', $request->semester)->where('guru_id', $request->guru)->first();
        if($check_guru){
            $guru = Guru::find($request->guru);
            return redirect()->route('wali-kelas.index')->with('error', 'Guru '.$guru->nama_guru.' sudah terdaftar');
        }

        $update = WaliKelas::find($id)->update([
            'semester_id' => $request->semester,
            // 'semester_id' => $semester->id,
            'guru_id' => $request->guru,
            'kelas_id' => $request->kelas,
        ]);
        
        return redirect()->route('wali-kelas.index')->with('status', 'Berhasil memperbarui data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WaliKelas  $walikelas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WaliKelas::find($id)->delete();
        return redirect()->route('wali-kelas.index')->with('status', 'Berhasil menghapus data');
    }

}
