<?php

namespace App\Http\Controllers;

use App\Models\Konfigurasi;
use App\Models\PersentaseNilai;
use App\Models\Semester;
use Illuminate\Http\Request;

class KonfigurasiController extends Controller
{
    public function setEnvironmentValue($envKey, $envValue)
    {
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);

        $str .= "\n"; // In case the searched variable is in the last line without \n
        $keyPosition = strpos($str, "{$envKey}=");
        $endOfLinePosition = strpos($str, PHP_EOL, $keyPosition);
        $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);
        $str = str_replace($oldLine, "{$envKey}={$envValue}", $str);
        $str = substr($str, 0, -1);

        $fp = fopen($envFile, 'w');
        fwrite($fp, $str);
        fclose($fp);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $konfigurasi = Konfigurasi::all();
        // $persentase = PersentaseNilai::get()->last();
        $persentase = PersentaseNilai::all();
        $semester = Semester::all();
        return view('data-master.konfigurasi', compact('konfigurasi','persentase','semester'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        if($request->app_name){
            // putenv ("APP_NAME='".$request->app_name."'");
            $this->setEnvironmentValue('APP_NAME', "'".$request->app_name."'");
        }
        if($request->school_name){
            $this->setEnvironmentValue('SCHOOL_NAME', "'".$request->school_name."'");
        }
        if($request->school_address){
            $this->setEnvironmentValue('SCHOOL_ADDRESS', "'".$request->school_address."'");
        }
        if($request->semester && $request->tugas){
            PersentaseNilai::create([
                'semester_id' => $request->semester,
                'tugas' => $request->tugas,
                'harian' => $request->harian,
                'uts' => $request->uts,
                'uas' => $request->uas,
            ]);
        }
        return redirect()->route('konfigurasi.index')->with('status', 'Berhasil mengupdate data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Konfigurasi  $konfigurasi
     * @return \Illuminate\Http\Response
     */
    public function show(Konfigurasi $konfigurasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Konfigurasi  $konfigurasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Konfigurasi $konfigurasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Konfigurasi  $konfigurasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Konfigurasi $konfigurasi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Konfigurasi  $konfigurasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Konfigurasi $konfigurasi)
    {
        //
    }
}
