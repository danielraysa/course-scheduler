<?php

namespace App\Http\Controllers;

use App\Models\TabuList;
use Illuminate\Http\Request;

class TabuListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TabuList  $tabuList
     * @return \Illuminate\Http\Response
     */
    public function show(TabuList $tabuList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TabuList  $tabuList
     * @return \Illuminate\Http\Response
     */
    public function edit(TabuList $tabuList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TabuList  $tabuList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TabuList $tabuList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TabuList  $tabuList
     * @return \Illuminate\Http\Response
     */
    public function destroy(TabuList $tabuList)
    {
        //
    }
}
