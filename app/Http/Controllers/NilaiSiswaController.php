<?php

namespace App\Http\Controllers;

use App\Helper\KalkulasiNilai;
use App\Models\NilaiSiswa;
use App\Models\JadwalPelajaran;
use App\Models\Guru;
use App\Models\MataPelajaran;
use App\Models\Semester;
use App\Models\Konstrain;
use App\Models\Siswa;
use App\Models\Kelas;
use App\Models\PenempatanKelas;
use App\Models\PersentaseNilai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NilaiSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $semester = Semester::first();
        // $semester = Semester::whereAktif()->first();;
        $kelas = Kelas::whereAktif()->get();
        $matpel = MataPelajaran::whereAktif()->get();
        $jadwal = JadwalPelajaran::where('semester_id', $semester->id)->get();
        $user = Auth::user();
        if ($user->data_guru) {
            $jadwal_guru = $jadwal->where('guru_id', $user->data_guru->id)->pluck('matpel_id');
            $kelas_guru = $jadwal->where('guru_id', $user->data_guru->id)->pluck('kelas_id');
            $matpel = $matpel->whereIn('id', $jadwal_guru);
            $kelas = $kelas->whereIn('id', $kelas_guru);
            // $kelas = Kelas::whereIn('id', $kelas_ajar_guru)->get();
            // $matpel = MataPelajaran::whereIn('id', $matpel_ajar_guru)->get();
        }
        $data['kelas'] = $kelas;
        $data['matpel'] = $matpel;
        if (isset($request->lihat)) {
            if($request->kelas && $request->matpel){

                $persentase = PersentaseNilai::first();
                $data['persentase'] = $persentase;
                $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get()->pluck('siswa_id');

                $siswa = Siswa::with(['data_nilai' => function($query) use ($semester, $request) {
                    $query->where('semester_id', $semester->id)->where('matpel_id', $request->matpel);
                }])->whereIn('id', $penempatan)->get();

                $data['siswa'] = $siswa;
                $data['request'] = $request;
            }
            return view('guru.penilaian-lihat', $data);
        }
        if (isset($request->ubah)) {
            if($request->kelas && $request->matpel){
                // $semester = Semester::whereAktif()->first();
                // $semester = Semester::first();
                $data['data_kelas'] = Kelas::find($request->kelas);
                $siswa = PenempatanKelas::with('data_siswa')->where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get();
                $kategori_nilai = NilaiSiswa::select('kategori', DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y") as tanggal'))->where('matpel_id', $request->matpel)->whereIn('siswa_id', $siswa->pluck('siswa_id'))->groupBy('kategori', DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y")'))->get();
                $data['nilai'] = $kategori_nilai;
                if($request->jenis_nilai){
                    $data_jenis = explode('_', $request->jenis_nilai);
                    $data_nilai = NilaiSiswa::where('matpel_id', $request->matpel)->whereIn('siswa_id', $siswa->pluck('siswa_id'))->where('kategori', $data_jenis[0])->whereRaw("DATE_FORMAT(created_at, '%d/%m/%Y') = '{$data_jenis[1]}'")->get();
                    // dd($data_jenis, $data_nilai);
                    $data['data_nilai'] = $data_nilai;
                }
            }
            $data['request'] = $request;
            return view('guru.penilaian-ubah', $data);
        }
        return view('guru.penilaian', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if($request->kelas){
            $semester = Semester::whereAktif()->first();
            $siswa = PenempatanKelas::with('data_siswa')->where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get();
            return view('guru.list-nilai', compact('siswa'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        // $guru = Auth::user();
        // $tanggal = date('Y-m-d');
        $semester = Semester::whereAktif()->first();
        // $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get()->pluck('siswa_id');
        // dd($penempatan);
        // $siswa = Siswa::whereIn('id', $penempatan)->get();
        $id_siswa = $request->id_siswa;
        $nilai = $request->nilai_siswa;
        for($i = 0; $i < count($id_siswa); $i++){
            NilaiSiswa::create([
                'semester_id' => $semester->id,
                'matpel_id' => $request->matpel,
                'siswa_id' => $id_siswa[$i],
                'kategori'=> $request->jenis_nilai,
                'nilai' => $nilai[$i],
            ]);
        }
        return redirect()->route('penilaian.index')->with('status', 'Berhasil menyimpan data nilai');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NilaiSiswa  $nilaiSiswa
     * @return \Illuminate\Http\Response
     */
    public function show(NilaiSiswa $nilaiSiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NilaiSiswa  $nilaiSiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(NilaiSiswa $nilaiSiswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NilaiSiswa  $nilaiSiswa
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, NilaiSiswa $nilaiSiswa)
    public function update($nilai, NilaiSiswa $nilaiSiswa)
    {
        if ($nilaiSiswa->nilai != $nilai) {
            $nilaiSiswa->update([
                'nilai' => $nilai
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NilaiSiswa  $nilaiSiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(NilaiSiswa $nilaiSiswa)
    {
        //
    }

    public function update_nilai(Request $request)
    {
        foreach ($request->id_nilai as $key => $value) {
            $this->update($request->nilai_siswa[$key], NilaiSiswa::find($value));
        }
        return redirect()->back()->with('status', 'Berhasil memperbarui data nilai');
    }

}
