<?php

namespace App\Http\Controllers;

use App\Models\Konstrain;
use Illuminate\Http\Request;

class KonstrainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $konstrain = Konstrain::select('max_jam_berurutan','max_guru_mengajar','max_bobot_matpel','max_iterasi')->distinct()->first();
        $konstrain_harian = Konstrain::all();
        return view('data-master.konstrain', compact('konstrain','konstrain_harian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $id = $request->id_hari;
        $max_jam_matpel = $request->max_jam_matpel;
        for($i = 0; $i < count($id); $i++){
            $update = Konstrain::find($id[$i])->update([
                'max_jam_matpel' => $max_jam_matpel[$i],
                'max_jam_berurutan' => $request->max_jam_berurutan,
                'max_guru_mengajar' => $request->max_guru_mengajar,
                'max_bobot_matpel' => $request->max_bobot_matpel,
                'max_iterasi' => $request->max_iterasi,
            ]);
        }
        return redirect()->route('konstrain.index')->with('status', 'Berhasil mengupdate data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Konstrain  $konstrain
     * @return \Illuminate\Http\Response
     */
    public function show(Konstrain $konstrain)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Konstrain  $konstrain
     * @return \Illuminate\Http\Response
     */
    public function edit(Konstrain $konstrain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Konstrain  $konstrain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Konstrain $konstrain)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Konstrain  $konstrain
     * @return \Illuminate\Http\Response
     */
    public function destroy(Konstrain $konstrain)
    {
        //
    }
}
