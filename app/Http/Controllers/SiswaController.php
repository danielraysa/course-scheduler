<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $siswa = Siswa::all();
        return view('data-master.siswa', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $new_user = User::create([
            'email' => $request->email,
            'name' => $request->nama_siswa,
            'password' => bcrypt($request->password),
        ]);
        $siswa = Siswa::create([
            'user_id' => $new_user->id,
            'nis' => $request->nis,
            'nama_siswa' => $request->nama_siswa,
            'status_siswa' => 'Aktif'
        ]);

        return redirect()->route('siswa.index')->with('status', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        $siswa->update([
            'nis' => $request->nis,
            'nama_siswa' => $request->nama_siswa,
        ]);
        // $user_siswa = $siswa->user_data;
        // if ($request->email != $siswa->user_data->email) {
            $siswa->data_user->update([
                'email' => $request->email,
                'name' => $request->nama_siswa,
            ]);
        // }
        return redirect()->route('siswa.index')->with('status', 'Berhasil memperbarui data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa)
    {
        $siswa->update([
            'status_siswa' => 'Tidak Aktif'
        ]);
        return redirect()->route('siswa.index')->with('status', 'Berhasil mengubah status siswa');

    }

    public function aktifkan($id)
    {
        Siswa::find($id)->update([
            'status_siswa' => 'Aktif'
        ]);
        return redirect()->route('siswa.index')->with('status', 'Berhasil mengubah status siswa');

    }
}
