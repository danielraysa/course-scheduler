<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Asrama;

class AsramaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $asrama = Asrama::all();
        return view('data-master.asrama', compact('asrama'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $simpan = Asrama::create([
            'nama_asrama' => $request->nama_asrama,
            'status_asrama' => 'Aktif'
        ]);
        return redirect()->route('asrama.index')->with('status', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asrama  $asrama
     * @return \Illuminate\Http\Response
     */
    public function show(Asrama $asrama)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asrama  $asrama
     * @return \Illuminate\Http\Response
     */
    public function edit(Asrama $asrama)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asrama  $asrama
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asrama $asrama)
    {
        //
        $update = Asrama::find($asrama->id)->update([
            'nama_asrama' => $request->nama_asrama,
        ]);
        return redirect()->route('asrama.index')->with('status', 'Berhasil mengubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asrama  $asrama
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asrama $asrama)
    {
        //
        $hapus = Asrama::find($asrama->id)->update([
            'status_asrama' => 'Tidak Aktif',
        ]);
        return redirect()->route('asrama.index')->with('status', 'Berhasil menghapus data');
    }
}
