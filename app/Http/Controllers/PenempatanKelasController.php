<?php

namespace App\Http\Controllers;

use App\Models\PenempatanKelas;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Asrama;
use App\Models\Semester;
use Illuminate\Http\Request;

class PenempatanKelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data['siswa'] = Siswa::all();
        $data['kelas'] = Kelas::all();
        $data['asrama'] = Asrama::all();
        $data['semester'] = Semester::all();
        if($request->kelas && $request->semester){
            $data['request'] = $request;
            $data['penempatan'] = PenempatanKelas::where('semester_id', $request->semester)->where('kelas_id', $request->kelas)->get();
        }
        return view('data-master.penempatan', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $kelas = Kelas::find($request->kelas);
        $cek_kuota = PenempatanKelas::where('semester_id', $request->semester)->where('kelas_id', $request->kelas)->get();
        if ($cek_kuota->count() >= $kelas->kuota_kelas) {
            return redirect()->route('penempatan.index')->with('error', 'Kelas '.$kelas->nama_kelas.' sudah terisi penuh');
        }

        PenempatanKelas::create([
            'semester_id' => $request->semester,
            'siswa_id' => $request->siswa,
            'kelas_id' => $request->kelas,
            'asrama_id' => $request->asrama,
        ]);
        return redirect()->route('penempatan.index')->with('status', 'Berhasil menyimpan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PenempatanKelas  $penempatanKelas
     * @return \Illuminate\Http\Response
     */
    public function show(PenempatanKelas $penempatanKelas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PenempatanKelas  $penempatanKelas
     * @return \Illuminate\Http\Response
     */
    public function edit(PenempatanKelas $penempatanKelas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PenempatanKelas  $penempatanKelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PenempatanKelas $penempatanKelas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PenempatanKelas  $penempatanKelas
     * @return \Illuminate\Http\Response
     */
    public function destroy(PenempatanKelas $penempatanKelas)
    {
        //
    }
}
