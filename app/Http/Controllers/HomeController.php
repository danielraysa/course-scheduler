<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guru;
use App\Models\JadwalPelajaran;
use App\Models\Kelas;
use App\Models\Semester;
use App\Models\Presensi;
use App\Models\PenempatanKelas;
use App\Models\Siswa;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->id == 1){
            $data_guru = Guru::all();
            $data_siswa = Siswa::all();
            $data_kelas = Kelas::where('status_kelas', 'Aktif')->get();
            return view('admin.home', compact('data_guru','data_siswa','data_kelas'));
        }
        if($user->data_guru){
            $data['jadwal'] = $this->jadwal_guru($user->data_guru);
            return view('guru.home', $data);
        }
        if($user->data_siswa){
            $presensi = Presensi::where('siswa_id', $user->data_siswa->id);
            $data['jadwal'] = $this->jadwal_siswa($user->data_siswa);
            $data['presensi_total'] = $presensi->count();
            $data['presensi_hadir'] = $presensi->where('status', 'H')->count();
            $data['presensi_alpha'] = $presensi->where('status', 'A')->count();
            return view('siswa.jadwal', $data);
        }
        return view('starter');
    }

    public function jadwal_siswa($siswa)
    {
        // $siswa = $user->data_siswa;
        $hari = $this->get_hari();
        $semester = Semester::whereAktif()->first();
        $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('siswa_id', $siswa->id)->first();
        // $jadwal = JadwalPelajaran::where('semester_id', $semester->id)->where('kelas_id', $penempatan->kelas_id)->orderBy('hari')->orderBy('jam_matpel')->get();
        $jadwal = JadwalPelajaran::where('semester_id', $semester->id)->where('kelas_id', $penempatan->kelas_id)->where('hari', $hari)->orderBy('jam_matpel')->get();
        return $jadwal;
    }

    public function jadwal_guru($guru)
    {
        // $siswa = $user->data_siswa;
        $hari = $this->get_hari();
        $semester = Semester::whereAktif()->first();
        // $jadwal = JadwalPelajaran::where('semester_id', $semester->id)->where('kelas_id', $penempatan->kelas_id)->orderBy('hari')->orderBy('jam_matpel')->get();
        $jadwal = JadwalPelajaran::where('semester_id', $semester->id)->where('guru_id', $guru->id)->where('hari', $hari)->orderBy('jam_matpel')->get();
        return $jadwal;
    }

    public function get_hari()
    {
        $hari_number = date('N');
        switch ($hari_number) {
            case 1:
                $hari = 'Senin';
                break;
            case 2:
                $hari = 'Selasa';
                break;
            case 3:
                $hari = 'Rabu';
                break;
            case 4:
                $hari = 'Kamis';
                break;
            case 5:
                $hari = 'Jumat';
                break;
            case 6:
                $hari = 'Sabtu';
                break;
            default:
                $hari = 'Minggu';
                break;
        }
        return $hari;
    }

}
