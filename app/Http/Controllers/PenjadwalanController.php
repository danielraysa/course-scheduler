<?php

namespace App\Http\Controllers;

use App\Models\JadwalPelajaran;
use App\Models\Guru;
use App\Models\MataPelajaran;
use App\Models\Semester;
use App\Models\Konstrain;
use App\Models\Siswa;
use App\Models\Kelas;
use Illuminate\Http\Request;

class PenjadwalanController extends Controller
{
    //
    public function index()
    {
        $data = $this->load_data_master();
        return view('data-master.jadwal', $data);
    }

    public function load_data_master()
    {
        $kelas = Kelas::where('status_kelas', 'Aktif')->get();
        $matpel = MataPelajaran::where('status_matpel', 'Aktif')->get();
        $guru = Guru::with('kompetensi.matpel_guru')->where('status_guru', 'Aktif')->get();
        $konstrain_harian = Konstrain::select('hari','max_jam_matpel')->get();
        $harian = Konstrain::select('hari','max_jam_matpel')->get();
        $konstrain = Konstrain::get()->first();
        return compact('kelas', 'guru', 'matpel', 'konstrain', 'harian');
    }

    public function index_old(Request $request)
    {
        // dd($request->all());
        $tabu_list = array();
        if($request->acak){
            $guru = Guru::with('kompetensi.matpel_guru')->inRandomOrder()->get();
        }else{
            $guru = Guru::with('kompetensi.matpel_guru')->get();
        }
        // dd($guru);
        $kelas = Kelas::all();
        $konstrain_harian = Konstrain::select('hari','max_jam_matpel')->get();
        $harian = Konstrain::select('hari','max_jam_matpel')->get();
        $konstrain = Konstrain::get()->first();
        $total_jadwal = array();
        // for($hari_ke = 0; $hari_ke < $konstrain->)
        //matriks 9 x 6 x jml kelas (4)
        // 9 x 24
        $matriks = array();
        $detail_matriks = array();
        $arr_guru = array();
        foreach($guru as $gr){
            // $matriks[1][1] = $gr->id;
            // echo $gr->nama_guru."<br>";
            for($x = 0; $x < $kelas->count(); $x++){
                for($y = 0; $y < $konstrain_harian->count(); $y++){
                    $arr_harian = array();
                    $max_bobot_matpel = 0;
                    // for($jam = 1; $jam <= $hari->max_jam_matpel; $jam++){
                    // for($jam = 0; $jam < 9; $jam++){
                    for($jam = 0; $jam < $konstrain_harian[$y]->max_jam_matpel; $jam++){
                        if(!isset($matriks[$jam][$y+($x*6)])){
                            /* $matriks[$jam][$y+($x*6)] = $gr->id;
                            array_push($arr_guru, $gr->id);
                            array_push($arr_harian, $gr->id); */
                            if(count(array_keys($arr_guru, $gr->id)) < $konstrain->max_guru_mengajar && count(array_keys($arr_harian, $gr->id)) < $konstrain->max_jam_berurutan){
                                $matriks[$jam][$y+($x*6)] = $gr;
                                $detail_matriks[$jam][$y+($x*6)] = [
                                    'hari' => $harian[$y]->hari,
                                    'jam_ke' => $jam+1,
                                    'guru' => $gr->nama_guru,
                                    // 'matpel' => $gr->kompetensi->matpel_guru->nama_matpel,
                                    'kelas' => $kelas[$x]->nama_kelas,
                                ];
                                array_push($arr_guru, $gr->id);
                                array_push($arr_harian, $gr->id);
                            }
                        }
                    }
                }
            }
        }
        // dd($detail_matriks);
        return view('penjadwalan', compact('matriks', 'detail_matriks', 'kelas', 'harian'));
    }

    public function store(Request $request)
    {
        $semester = Semester::whereAktif()->first();
        // dd($request->all(), $semester);
        $check = JadwalPelajaran::where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->where('jam_matpel', $request->jam)->where('hari', $request->hari)->first();
        if($check){
            return redirect()->route('penjadwalan.index')->with('error', 'Data jadwal sudah ada');
        }
        JadwalPelajaran::create([
            'semester_id' => $semester->id,
            'kelas_id' => $request->kelas,
            'matpel_id' => $request->matpel,
            'guru_id' => $request->guru,
            'jam_matpel' => $request->jam,
            'hari' => $request->hari,
        ]);
        return redirect()->route('penjadwalan.index')->with('status', 'Berhasil menyimpan data jadwal');
    }

    public function showList(Request $request)
    {
        if($request->kelas && $request->hari){
            $data = $this->load_data_master();
            $data['jadwal_kelas'] = JadwalPelajaran::where('kelas_id', $request->kelas)->where('hari', $request->hari)->orderBy('jam_matpel')->get();
            return view('data-master.jadwal-tabel', $data);
        }
    }

    public function update(Request $request, $id)
    {
        // $semester = Semester::first();
        // dd($request->all(), $semester);
        JadwalPelajaran::find($id)->update([
            // 'semester_id' => $request->semester,
            // 'semester_id' => $semester->id,
            'kelas_id' => $request->kelas,
            'matpel_id' => $request->matpel,
            'guru_id' => $request->guru,
            'jam_matpel' => $request->jam,
            'hari' => $request->hari,
        ]);
        return redirect()->route('penjadwalan.index')->with('status', 'Berhasil memperbarui data jadwal');
    }

    public function destroy($id)
    {
        JadwalPelajaran::find($id)->delete();
        return redirect()->route('penjadwalan.index')->with('status', 'Berhasil menghapus data jadwal');
    }
}
