<?php

namespace App\Http\Controllers;

use App\Models\JadwalPelajaran;
use App\Models\Guru;
use App\Models\MataPelajaran;
use App\Models\Semester;
use App\Models\Konstrain;
use App\Models\Siswa;
use App\Models\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JadwalPelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        if($user->data_guru){
            $data_guru = $user->data_guru;
            $jadwal = JadwalPelajaran::where('guru_id', $data_guru->id)->get();
            return view('siswa.penjadwalan', compact('jadwal'));
        }
        $siswa = $user->data_siswa;
        $jadwal = JadwalPelajaran::where('guru_id', $siswa->id)->get();
        return view('siswa.penjadwalan', compact('jadwal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JadwalPelajaran  $jadwalPelajaran
     * @return \Illuminate\Http\Response
     */
    public function show(JadwalPelajaran $jadwalPelajaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JadwalPelajaran  $jadwalPelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit(JadwalPelajaran $jadwalPelajaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JadwalPelajaran  $jadwalPelajaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JadwalPelajaran $jadwalPelajaran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JadwalPelajaran  $jadwalPelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(JadwalPelajaran $jadwalPelajaran)
    {
        //
    }
}
