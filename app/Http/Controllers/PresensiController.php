<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Presensi;
use App\Models\Kelas;
use App\Models\MataPelajaran;
use App\Models\Guru;
use App\Models\Siswa;
use App\Models\Semester;
use App\Models\PenempatanKelas;
use App\Models\Jadwal;
use App\Models\JadwalPelajaran;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PresensiController extends Controller
{
    //
    public function index()
    {
        $hari = $this->get_hari();
        $user = Auth::user();
        if($user->data_siswa){
            $year = date('Y');
            $month = date('m');
            $presensi = Presensi::where('siswa_id', $user->data_siswa->id)->get();
            // $presensi = Presensi::whereYear('tanggal', $year)->whereMonth('tanggal', $month)->where('siswa_id', $user->data_siswa->id)->get();
            // dd($presensi);
            $presensi_total = $presensi->count();
            $presensi_hadir = $presensi->where('status', 'H')->count();
            $presensi_alpha = $presensi->where('status', 'A')->count();
            $presensi_ijin = $presensi->where('status', 'I')->count();
            $presensi_sakit = $presensi->where('status', 'S')->count();
            $data = [
                'presensi_total' => $presensi_total,
                'presensi_hadir' => $presensi_hadir,
                'presensi_alpha' => $presensi_alpha,
                'presensi_ijin' => $presensi_ijin,
                'presensi_sakit' => $presensi_sakit,
                'data_chart' => [
                    'data' => [$presensi_hadir, $presensi_alpha, $presensi_ijin, $presensi_sakit],
                    'backgroundColor' => ["rgb(155, 205, 86)","rgb(54, 162, 235)","rgb(255, 205, 86)", "rgb(255, 99, 132)"],
                    'labels' => ['Hadir', 'Alpha', 'Ijin', 'Sakit'] 
                ]
            ];
            return view('siswa.presensi', $data);
        }
        $kelas = Kelas::where('status_kelas', 'Aktif')->get();
        $matpel = MataPelajaran::where('status_matpel', 'Aktif')->get();
        if($user->data_guru){
            $jadwal = JadwalPelajaran::where('hari', $hari)->where('guru_id', $user->data_guru->id)->get();
            // $kelas = Kelas::where('status_kelas', 'Aktif')->whereIn('id', $jadwal->pluck('kelas_id'))->get();
            // $matpel = MataPelajaran::where('status_matpel', 'Aktif')->whereIn('id', $jadwal->pluck('matpel_id'))->get();
            return view('guru.presensi', compact('hari','jadwal'));
        }
        return view('guru.presensi', compact('kelas','matpel','hari'));
    }

    public function create(Request $request)
    {
        if($request->kelas){
            $semester = Semester::whereAktif()->first();
            // $penempatan = PenempatanKelas::with('data_siswa','data_asrama')->where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get()->pluck('siswa_id');
            $penempatan = PenempatanKelas::with('data_siswa','data_asrama')->where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get();
            // dd($penempatan);
            // $siswa = Siswa::whereIn('id', $penempatan)->get();
            // return view('guru.list-siswa', compact('siswa', 'penempatan', 'request'));
            return view('guru.list-siswa', compact('penempatan', 'request'));
        }
        if($request->jadwal){
            $semester = Semester::whereAktif()->first();
            $jadwal = JadwalPelajaran::find($request->jadwal);
            $penempatan = PenempatanKelas::with('data_siswa','data_asrama')->where('semester_id', $semester->id)->where('kelas_id', $jadwal->kelas_id)->get();
            return view('guru.list-siswa', compact('penempatan', 'request'));
        }
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $guru = Auth::user();
        $tanggal = date('Y-m-d');
        $semester = Semester::whereAktif()->first();
        // $semester = Semester::first();
        /* $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get()->pluck('siswa_id');
        // dd($penempatan);
        $siswa = Siswa::whereIn('id', $penempatan)->get();
        foreach($siswa as $sis){
            $status = 'A'; // alpha atau tidak masuk
            if(in_array($sis->id, $request->kehadiran)){
                $status = 'H'; // hadir
            }
            Presensi::create([
                'guru_id' => $guru->id,
                'matpel_id' => $request->matpel,
                'kelas_id' => $request->kelas,
                'siswa_id' => $sis->id,
                'status' => $status,
                'tanggal' => $tanggal
            ]);
        } */
        if ($request->jadwal) {
            $jadwal = JadwalPelajaran::find($request->jadwal);
            $jadwal_id = $jadwal->id;
            $kelas_id = $jadwal->kelas_id;
            $matpel_id = $jadwal->matpel_id;
        } else {
            $jadwal_id = null;
            $kelas_id = $request->matpel;
            $matpel_id = $request->kelas;
        }
        for ($i = 0; $i < count($request->id_siswa); $i++) {
            Presensi::create([
                'jadwal_id' => $jadwal_id,
                'guru_id' => $guru->id,
                'matpel_id' => $matpel_id,
                'kelas_id' => $kelas_id,
                'siswa_id' => $request->id_siswa[$i],
                'status' => $request->kehadiran[$i],
                'tanggal' => $tanggal,
            ]);
        }
        return redirect()->route('presensi.index')->with('status', 'Berhasil menyimpan data presensi');
    }

    public function showList(Request $request)
    {
        $user = Auth::user();
        if ($user->data_siswa) {
            return $this->index();
        }
        $data_guru = $user->data_guru;
        $jadwal_guru = JadwalPelajaran::where('guru_id', $data_guru->id)->get();
        $data['kelas'] = Kelas::all();
        $data['matpel'] = MataPelajaran::whereAktif()->get();
        if($request->kelas && $request->matpel){
            $data['request'] = $request;
            // $data['list_presensi'] = Presensi::selectRaw('DATE(presensi_siswa.tanggal) as tgl, kelas.nama_kelas')->join('kelas','presensi_siswa.kelas_id','=','kelas.id')->groupByRaw('DATE(presensi_siswa.tanggal)')->groupByRaw('kelas.nama_kelas')->get();
            $data['list_presensi'] = Presensi::with('data_siswa')->where('kelas_id', $request->kelas)->where('matpel_id', $request->matpel)->whereRaw("DATE(tanggal) = '".$request->tanggal."'")->get();
            // dd($data);
        }
        return view('guru.presensi-kelas', $data);
    }

    public function susulan(Request $request)
    {
        $user = Auth::user();
        $data['kelas'] = Kelas::all();
        $data['matpel'] = MataPelajaran::whereAktif()->get();
        // $jadwal = JadwalPelajaran::where('hari', $this->get_hari($request->tanggal))->where('kelas_id', $request->kelas)->get();
        $data_guru = $user->data_guru;
        /* $jadwal = Presensi::with('data_kelas')->select('jadwal_id','tanggal','kelas_id')->where('guru_id', $data_guru->id)->groupBy('jadwal_id','tanggal','kelas_id')->get();

        $data['jadwal'] = $jadwal; */
        // if($request->tanggal && $request->kelas && $request->matpel){
        if($request->tanggal){
            $jadwal = JadwalPelajaran::where('hari', $this->get_hari($request->tanggal))->where('guru_id', $data_guru->id)->get();
            // dd($jadwal, $this->get_hari($request->tanggal), $request->kelas);
            $data['jadwal'] = $jadwal;
            $data['request'] = $request;
            // $data['list_presensi'] = Presensi::selectRaw('DATE(presensi_siswa.tanggal) as tgl, kelas.nama_kelas')->join('kelas','presensi_siswa.kelas_id','=','kelas.id')->groupByRaw('DATE(presensi_siswa.tanggal)')->groupByRaw('kelas.nama_kelas')->get();
            if($request->jadwal){
                $jadwal_terpilih = JadwalPelajaran::find($request->jadwal);
                $data['kelas_terpilih'] = Kelas::find($jadwal_terpilih->kelas_id);
                $list_presensi = Presensi::with('data_siswa')->where('kelas_id', $jadwal_terpilih->kelas_id)->where('matpel_id', $jadwal_terpilih->matpel_id)->whereRaw("DATE(tanggal) = '".$request->tanggal."'")->get();
                if($list_presensi->count() == 0){
                    $semester = Semester::whereAktif()->first();
                    $data['data_siswa'] = PenempatanKelas::with('data_siswa','data_asrama')->where('semester_id', $semester->id)->where('kelas_id', $jadwal_terpilih->kelas_id)->get();
                }else{
                    $data['list_presensi'] = $list_presensi;
                }
                // dd($data);
            }
        }
        return view('guru.susulan-new', $data);
    }

    public function simpan_susulan(Request $request)
    {
        $guru = Auth::user();
        // $user = Auth::user();
        // $guru = $user->data_guru;
        $jadwal_terpilih = JadwalPelajaran::find($request->jadwal);
        $list_presensi = Presensi::with('data_siswa')->where('kelas_id', $jadwal_terpilih->kelas_id)->where('matpel_id', $jadwal_terpilih->matpel_id)->whereRaw("DATE(tanggal) = '".$request->tanggal."'")->get();
        // dd($list_presensi, $guru);
        if ($list_presensi->count() == 0) {
            // belum ada presensi, presensi baru
            for($i = 0; $i < count($request->kehadiran); $i++) {
                Presensi::create([
                    'jadwal_id' => $request->jadwal,
                    'guru_id' => $guru->id,
                    'matpel_id' => $jadwal_terpilih->matpel_id,
                    'kelas_id' => $jadwal_terpilih->kelas_id,
                    'siswa_id' => $request->id_siswa[$i],
                    'status' => $request->kehadiran[$i],
                    'tanggal' => $request->tanggal,
                ]);
            }
        } else {
            // presensi sudah ada, diupdate/susulan
            for($i = 0; $i < $list_presensi->count(); $i++) {
                if($list_presensi[$i]->status != $request->kehadiran[$i]) {
                    // dd($list_presensi[$i]->status, $request->kehadiran[$i]);
                    $upd_status = $list_presensi[$i]->update([
                        'status' => $request->kehadiran[$i],
                    ]);
                }
            }
        }
        return redirect()->back()->with('status', 'Berhasil menyimpan data presensi susulan');
    }
    
    public function list_presensi_siswa(Request $request)
    {
        $siswa = Auth::user()->data_siswa;
        $cek_presensi = Presensi::where('siswa_id', $siswa->id)->where('tanggal', $request->tanggal)->get();
        // dd($cek_presensi);
        $tanggal = date('d/m/Y', strtotime($request->tanggal));
        return view('siswa.list-presensi', compact('cek_presensi','tanggal'));
    }

    public function get_hari($date = null)
    {
        $hari_number = date('N');
        if ($date != null) {
            $hari_number = date('N', strtotime($date));
        }
        switch ($hari_number) {
            case 1:
                $hari = 'Senin';
                break;
            case 2:
                $hari = 'Selasa';
                break;
            case 3:
                $hari = 'Rabu';
                break;
            case 4:
                $hari = 'Kamis';
                break;
            case 5:
                $hari = 'Jumat';
                break;
            case 6:
                $hari = 'Sabtu';
                break;
            default:
                $hari = 'Minggu';
                break;
        }
        return $hari;
    }

}
