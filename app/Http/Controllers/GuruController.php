<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\MataPelajaran;
use App\Models\KompetensiGuru;
use App\Models\User;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $guru = Guru::with('data_user','kompetensi.matpel_guru')->get();
        $matpel = MataPelajaran::all();
        return view('data-master.guru', compact('guru','matpel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_guru' => 'required|unique:guru'
        ]);
        // dd($request->all());
        if($request->user_create){
            $request->validate([
                'nama_guru' => 'required|unique:guru',
                'username' => 'required|unique:users,email'
            ]);
            $user = User::create([
                'name' => $request->nama_guru,
                'email' => $request->username,
                'password' => bcrypt($request->password),
            ]);
        }
        $guru = Guru::create([
            'user_id' => isset($user) ? $user->id : null,
            'nip' => $request->nip,
            'nama_guru' => $request->nama_guru,
            'status_guru' => Guru::AKTIF,
        ]);
        if($request->matpel){
            $kompetensi = KompetensiGuru::create([
                'guru_id' => $guru->id,
                'matpel_id' => $request->matpel,
            ]);
        }
        return redirect()->route('guru.index')->with('status', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function show(Guru $guru)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function edit(Guru $guru)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Guru $guru)
    {
        //
        $guru->update([
            'nip' => $request->nip,
            'nama_guru' => $request->nama_guru,
        ]);
        if($request->user_edit){
            User::find($guru->user_id)->update([
                'email' => $request->username,
                'password' => bcrypt($request->password),
            ]);
        }
        if($request->user_create || ($guru->user_id == null && $request->username != null && $request->password != null)){
            $user = User::create([
                'name' => $request->nama_guru,
                'email' => $request->username,
                'password' => bcrypt($request->password),
            ]);
            $guru->update([
                'user_id' => $user->id
            ]);
        }
        /* if($guru->kompetensi == null){
            $kompetensi = KompetensiGuru::create([
                'guru_id' => $guru->id,
                'matpel_id' => $request->matpel,
            ]);
        }else{
            $kompetensi = KompetensiGuru::where('guru_id', $guru->id)->update([
                'matpel_id' => $request->matpel,
            ]);
        } */
        return redirect()->route('guru.index')->with('status', 'Berhasil memperbarui data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guru $guru)
    {
        //
        $guru->update([
            'status_guru' => Guru::TIDAK_AKTIF,
        ]);
        return redirect()->route('guru.index')->with('status', 'Berhasil menghapus data');
    }

    public function aktifkan($id)
    {
        Guru::find($id)->update([
            'status_guru' => Guru::AKTIF,
        ]);
        return redirect()->route('guru.index')->with('status', 'Berhasil mengaktifkan data');
    }
}
