<?php

namespace App\Http\Controllers;

use App\Models\PersentaseNilai;
use App\Models\Semester;
use Illuminate\Http\Request;

class SemesterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $semester = Semester::with('persentase_nilai')->get();
        return view('data-master.semester', compact('semester'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (($request->harian + $request->tugas + $request->uts + $request->uas) != 100) {
            return redirect()->route('semester.index')->with('error', 'Jumlah persentase tidak mencapai 100%');
        }

        $semester = Semester::create([
            'periode' => $request->periode,
            'semester' => $request->semester,
            'status' => Semester::AKTIF,
        ]);
        $persentase = PersentaseNilai::create([
            'semester_id' => $semester->id,
            'harian' => $request->harian,
            'tugas' => $request->tugas,
            'uts' => $request->uts,
            'uas' => $request->uas,
        ]);
        return redirect()->route('semester.index')->with('status', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Semester  $semester
     * @return \Illuminate\Http\Response
     */
    public function show(Semester $semester)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Semester  $semester
     * @return \Illuminate\Http\Response
     */
    public function edit(Semester $semester)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Semester  $semester
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Semester $semester)
    {
        dd($request->all(), $semester);
        if (($request->harian + $request->tugas + $request->uts + $request->uas) != 100) {
            return redirect()->route('semester.index')->with('error', 'Jumlah persentase tidak mencapai 100%');
        }
        $semester->update([
            'periode' => $request->periode,
            'semester' => $request->semester,
        ]);
        PersentaseNilai::where('semester_id', $semester->id)->update([
            'harian' => $request->harian,
            'tugas' => $request->tugas,
            'uts' => $request->uts,
            'uas' => $request->uas,
        ]);
        return redirect()->route('semester.index')->with('status', 'Berhasil menonaktifkan data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Semester  $semester
     * @return \Illuminate\Http\Response
     */
    public function destroy(Semester $semester)
    {
        $semester->update([
            'status' => Semester::NONAKTIF,
        ]);
        return redirect()->route('semester.index')->with('status', 'Berhasil menonaktifkan data');
    }
}
