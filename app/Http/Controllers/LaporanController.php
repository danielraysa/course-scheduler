<?php

namespace App\Http\Controllers;

use App\Models\NilaiSiswa;
use App\Models\JadwalPelajaran;
use App\Models\Guru;
use App\Models\MataPelajaran;
use App\Models\Semester;
use App\Models\Konstrain;
use App\Models\Siswa;
use App\Models\Kelas;
use App\Models\PenempatanKelas;
use App\Models\PersentaseNilai;
use App\Models\Presensi;
use Illuminate\Http\Request;
use App\Helper\KalkulasiNilai;
use App\Models\WaliKelas;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller
{
    //
    public function index()
    {
        $semester = Semester::all();
        $bulan = date('m');
        return view('laporan.index');
    }

    public function raport(Request $request)
    {
        $user = Auth::user();
        $data['semester'] = Semester::all();
        $data['matpel'] = MataPelajaran::all();
        $kelas = Kelas::all();
        if ($user->data_guru->wali_kelas) {
            $data_guru = $user->data_guru;
            $kelas_wali = WaliKelas::where('guru_id', $data_guru->id)->get();
            $kelas = $kelas->whereIn('id', $kelas_wali->pluck('kelas_id'));
        }
        $data['kelas'] = $kelas;
        $persentase = PersentaseNilai::first();
        $data['persentase'] = $persentase;
        if($request->kelas && $request->matpel && $request->semester){
            $data['data_kelas'] = Kelas::find($request->kelas);
            $semester = Semester::find($request->semester);
            $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('kelas_id', $request->kelas)->get()->pluck('siswa_id');
            // dd($penempatan);
            $siswa = Siswa::with(['data_nilai' => function($query) use ($semester, $request) {
                $query->where('semester_id', $semester->id)->where('matpel_id', $request->matpel);
            }])->whereIn('id', $penempatan)->get();
            $data_nilai = collect();
            foreach($siswa as $item){
                $nilai = KalkulasiNilai::averageNilai($item, $persentase);
                $data_nilai->push($nilai);
            }
            /* $_nilai = NilaiSiswa::where('matpel_id', $request->matpel)->whereIn('id', $penempatan)->get();
            $data['rata_nilai'] = $_nilai->average('nilai');
            $data['nilai_tertinggi'] = $_nilai->max('nilai');
            $data['nilai_terendah'] = $_nilai->min('nilai'); */
            $data['rata_nilai'] = round($data_nilai->average('nilai_akhir'), 2);
            $data['nilai_tertinggi'] = round($data_nilai->max('nilai_akhir'), 2);
            $data['nilai_terendah'] = round($data_nilai->min('nilai_akhir'), 2);
            $data['siswa'] = $siswa;
            $data['request'] = $request;
            if ($request->cetak) {
                $data['kelas'] = Kelas::with('jns_kelas')->find($request->kelas);
                $data['matpel'] = MataPelajaran::find($request->matpel);
                $pdf = Pdf::loadView('pdf.nilai', $data);
                return $pdf->stream();
            }
        }

        return view('laporan.nilai', $data);
    }

    public function presensi(Request $request)
    {
        $user = Auth::user();
        if ($user->data_guru == null || $user->data_guru->wali_kelas == null) {
            return redirect('home');
        }
        $data_wali = $user->data_guru->wali_kelas;
        $bulan = date('m');
        $tahun = date('Y');
        $filter = $tahun."-".$bulan;
        if($request->bulan){
            $filter = $request->bulan;
            $data = explode('-', $filter);
            $tahun = $data[0];
            $bulan = $data[1];
        }
        $semester = Semester::first();
        $kelas = Kelas::find($data_wali->kelas_id);
        $penempatan = PenempatanKelas::where('semester_id', $semester->id)->where('kelas_id', $data_wali->kelas_id)->get()->pluck('siswa_id');
        $list_siswa = Siswa::whereIn('id', $penempatan->toArray())->get();
        $presensi = Presensi::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->where('kelas_id', $data_wali->kelas_id)->whereIn('siswa_id', $penempatan->toArray())->get();
        // $presensi_kelas = Presensi::select('kelas_id', DB::raw('count(*) as total'))->whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->groupBy('kelas_id')->get();
        $presensi_kelas = Presensi::select('kelas_id')->whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->groupBy('kelas_id')->get();
        $data_presensi = collect($list_siswa)->map(function($item) use($bulan, $tahun, $presensi){
            $hadir = $presensi->where('siswa_id', $item->id)->where('status', 'H')->count();
            $alpha = $presensi->where('siswa_id', $item->id)->where('status', 'A')->count();
            $ijin = $presensi->where('siswa_id', $item->id)->where('status', 'I')->count();
            $sakit = $presensi->where('siswa_id', $item->id)->where('status', 'S')->count();
            $item->hadir = $hadir;
            $item->alpha = $alpha;
            $item->ijin = $ijin;
            $item->sakit = $sakit;
            return $item;
        });

        $total = $presensi->count();
        $hadir = $presensi->where('status', 'H')->count();
        $alpha = $presensi->where('status', 'A')->count();
        $ijin = $presensi->where('status', 'I')->count();
        $sakit = $presensi->where('status', 'S')->count();
        $presentasi_hadir = 0;
        $presentasi_alpha = 0;
        $presentasi_ijin = 0;
        $presentasi_sakit = 0;
        if($total != 0){
            $presentasi_hadir = round(($hadir/$total) * 100, 2);
            $presentasi_alpha = round(($alpha/$total) * 100, 2);
            $presentasi_ijin = round(($ijin/$total) * 100, 2);
            $presentasi_sakit = round(($sakit/$total) * 100, 2);
        }
        $data = compact('bulan','tahun','semester','kelas','presentasi_hadir','presentasi_alpha','presentasi_ijin','presentasi_sakit','filter','data_presensi');
        if ($request->cetak) {
            $pdf = Pdf::loadView('pdf.presensi', $data);
            return $pdf->stream();
        }
        return view('laporan.presensi', $data);
    }
}
