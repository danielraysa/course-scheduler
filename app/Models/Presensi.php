<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Siswa;

class Presensi extends Model
{
    //
    const HADIR = 'H';
    const IJIN = 'I';
    const SAKIT = 'S';
    const ALPHA = 'A';

    protected $table = 'presensi_siswa';
    protected $guarded = [];

    /**
     * Get the data_siswa associated with the Presensi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data_siswa()
    {
        return $this->hasOne(Siswa::class, 'id', 'siswa_id');
    }

    /**
     * Get the data_jadwal associated with the Presensi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data_jadwal()
    {
        return $this->hasOne(JadwalPelajaran::class, 'id', 'jadwal_id');
    }

    /**
     * Get the data_matpel associated with the Presensi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data_matpel()
    {
        return $this->hasOne(MataPelajaran::class, 'id', 'matpel_id');
    }

    public function status_kehadiran()
    {
        $text = '';
        switch ($this->status) {
            case self::HADIR:
                $text = 'Hadir';
                break;
            case self::IJIN:
                $text = 'Ijin';
                break;
            case self::SAKIT:
                $text = 'Sakit';
                break;
            case self::ALPHA:
                $text = 'Alpha';
                break;
            default:
                # code...
                break;
        }
        return $text;
    }
}
