<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    //
    const AKTIF = 'Aktif';
    const TIDAK_AKTIF = 'Tidak Aktif';

    protected $table = 'guru';
    protected $guarded = [];

    public function data_user()
    {
        // return $this->belongsTo('App\Models\User', 'user_id', 'id');
        return $this->belongsTo(User::class, 'user_id');
    }

    public function kompetensi()
    {
        return $this->hasOne('App\Models\KompetensiGuru', 'guru_id', 'id');
    }

    /**
     * Get the wali_kelas associated with the Guru
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function wali_kelas()
    {
        return $this->hasOne(WaliKelas::class);
    }
}
