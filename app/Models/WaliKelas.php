<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaliKelas extends Model
{
    //
    protected $table = 'wali_kelas';
    protected $guarded = [];

    public function data_semester()
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }

    public function data_guru()
    {
        return $this->belongsTo(Guru::class, 'guru_id');
    }

    public function data_kelas()
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }
}
