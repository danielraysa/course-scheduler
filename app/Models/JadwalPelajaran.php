<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalPelajaran extends Model
{
    //
    const AKTIF = 'Aktif';
    const TIDAK_AKTIF = 'Tidak Aktif';

    protected $table = 'jadwal_pelajaran';
    protected $guarded = [];

    public function data_kelas()
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function data_guru()
    {
        return $this->belongsTo(Guru::class, 'guru_id');
    }

    public function data_matpel()
    {
        return $this->belongsTo(MataPelajaran::class, 'matpel_id');
    }

}
