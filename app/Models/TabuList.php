<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TabuList extends Model
{
    //
    protected $table = 'tabu_list';
    protected $guarded = [];
}
