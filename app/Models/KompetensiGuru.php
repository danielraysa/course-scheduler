<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KompetensiGuru extends Model
{
    //
    protected $table = 'kompetensi_guru';
    protected $guarded = [];

    public function matpel_guru()
    {
        return $this->hasOne('App\Models\MataPelajaran', 'id', 'matpel_id');
    }
}
