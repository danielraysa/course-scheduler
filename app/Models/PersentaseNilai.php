<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Semester;
class PersentaseNilai extends Model
{
    //
    protected $table = 'persentase_nilai';
    protected $guarded = [];

    public function data_semester()
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }
}
