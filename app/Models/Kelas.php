<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    //
    const AKTIF = 'Aktif';
    const TIDAK_AKTIF = 'Tidak Aktif';

    protected $table = 'kelas';
    protected $guarded = [];

    public function jns_kelas()
    {
       return $this->belongsTo('App\Models\JenisKelas', 'jenis_id', 'id');
    }

    public function scopeWhereAktif($query)
    {
        return $query->where('status_kelas', 'Aktif');
    }
}
