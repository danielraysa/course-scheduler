<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PenempatanKelas;
use App\Models\NilaiSiswa;

class Siswa extends Model
{
    //
    const AKTIF = 'Aktif';
    const TIDAK_AKTIF = 'Tidak Aktif';

    protected $table = 'siswa';
    protected $guarded = [];

    public function data_user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function data_penempatan()
    {
        // return $this->belongsTo(PenempatanKelas::class, 'id', 'siswa_id');
        return $this->hasOne(PenempatanKelas::class, 'siswa_id');
    }

    public function data_nilai()
    {
        return $this->hasMany(NilaiSiswa::class, 'siswa_id');
    }
}
