<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Kelas;
use App\Models\Asrama;
use App\Models\Siswa;
use App\Models\Semester;
class PenempatanKelas extends Model
{
    //
    protected $table = 'penempatan_kelas';
    protected $guarded = [];

    public function data_semester()
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }

    public function data_kelas()
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function data_asrama()
    {
        return $this->belongsTo(Asrama::class, 'asrama_id');
    }

    public function data_siswa()
    {
        return $this->belongsTo(Siswa::class, 'siswa_id');
    }
}
