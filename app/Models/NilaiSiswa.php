<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NilaiSiswa extends Model
{
    //
    protected $table = 'nilai_siswa';
    protected $guarded = [];

    /**
     * Get the data_siswa associated with the Presensi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data_siswa()
    {
        return $this->hasOne(Siswa::class, 'id', 'siswa_id');
    }

    /**
     * Get the data_matpel associated with the Presensi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data_matpel()
    {
        return $this->hasOne(MataPelajaran::class, 'id', 'matpel_id');
    }
}
