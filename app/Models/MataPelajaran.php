<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model
{
    //
    const AKTIF = 'Aktif';
    const TIDAK_AKTIF = 'Tidak Aktif';

    protected $table = 'mata_pelajaran';
    protected $guarded = [];

    /* public function newQuery()
    {
        return parent::newQuery()->where('status_matpel', '!=', 'Tidak Aktif');
    } */
    public function scopeWhereAktif($query)
    {
        return $query->where('status_matpel', 'Aktif');
    }

    public function data_nilai()
    {
        return $this->hasMany(NilaiSiswa::class, 'matpel_id');
    }
}
