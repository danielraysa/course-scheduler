<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    //
    const AKTIF = 'A';
    const NONAKTIF = 'N';

    protected $table = 'semester';
    protected $guarded = [];

    public function jenis_semester()
    {
        return $this->semester == 1 ? 'Ganjil' : 'Genap';
    }

    public function scopeWhereAktif($query)
    {
        return $query->where('status', self::AKTIF);
    }

    /**
     * Get the persentase_nilai associated with the Semester
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function persentase_nilai()
    {
        return $this->hasOne(PersentaseNilai::class, 'semester_id');
    }
}
