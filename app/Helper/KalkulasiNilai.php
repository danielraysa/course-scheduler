<?php

namespace App\Helper;

use NumberFormatter;

class KalkulasiNilai {

    /**
     * @param int $value
     * @return string
     */
    public static function numberSpellout($value)
    {
        $f = new NumberFormatter("ID-id", NumberFormatter::SPELLOUT);
        return ucwords($f->format($value));
    }

    /**
     * @param MataPelajaran $item
     * @param PersentaseNilai $persentase
     * @return array
     */
    public static function averageNilai($item, $persentase)
    {
        $nilai_tugas = [
            'total' => 0,
            'count' => 0,
        ];    
        $nilai_harian = [
            'total' => 0,
            'count' => 0,
        ];    
        $nilai_uts = [
            'total' => 0,
            'count' => 0,
        ];    
        $nilai_uas = [
            'total' => 0,
            'count' => 0,
        ];
        $rata_tugas = 0;
        $rata_harian = 0;
        $rata_uts = 0;
        $rata_uas = 0;
        $nilai_akhir = 0;
        foreach ($item->data_nilai as $nilai){
            switch($nilai->kategori){
                case 'uas':
                    $nilai_uas['total'] += $nilai->nilai;
                    $nilai_uas['count'] += 1;
                    break;
                case 'uts':
                    $nilai_uts['total'] += $nilai->nilai;
                    $nilai_uts['count'] += 1;
                    // array_push($nilai_uts, $nilai->nilai);
                    break;
                case 'harian':
                    $nilai_harian['total'] += $nilai->nilai;
                    $nilai_harian['count'] += 1;
                    // array_push($nilai_harian, $nilai->nilai);
                    break;
                default:
                    $nilai_tugas['total'] += $nilai->nilai;
                    $nilai_tugas['count'] += 1;
                    // array_push($nilai_tugas, $nilai->nilai);
                    break;
            }
        }
        $rata_tugas = $nilai_tugas['count'] == 0 ? 0 : $nilai_tugas['total']/$nilai_tugas['count'];
        $rata_harian = $nilai_harian['count'] == 0 ? 0 : $nilai_harian['total']/$nilai_harian['count'];
        $rata_uts = $nilai_uts['count'] == 0 ? 0 : $nilai_uts['total']/$nilai_uts['count'];
        $rata_uas = $nilai_uas['count'] == 0 ? 0 : $nilai_uas['total']/$nilai_uas['count'];
        $nilai_akhir = ($rata_tugas * $persentase->tugas / 100) + ($rata_harian * $persentase->harian / 100) + ($rata_uts * $persentase->uts / 100) + ($rata_uas * $persentase->uas / 100);
        return compact('rata_tugas','rata_harian','rata_uts','rata_uas','nilai_akhir');
    }
}